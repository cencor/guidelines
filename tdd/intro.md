# Test-Driven Development 

## ¿Qué es Test-Driven Development ?

Test-Driven Development (TDD) es una técnica de programación que invierte la secuencia usual de escribir código y probarlo. Esta técnica requiere escribir una prueba (preferentemente unitarias) sin siquiera escribir una línea de código que satisfaga el resultado. En otras palabras, lo primero que se escribe es la prueba que verificará que nuestro desarrollo es correcto. Hacerlo de este modo trae los siguientes beneficios para los programadores:

* Trabajar con confianza.
* Trabajar en una serie de pasos logrables en vez de atacar un gran problema en un solo intento.
* Asegurar que el diseño del software cumple con las necesidades del código.
* Dejar como respaldo un conjunto de pruebas que ayudan a conservar la integridad del código. Este conjunto de pruebas se convierten inmediatamente en un conjunto de pruebas de regresión, donde ante cualquier cambio podemos recibir retroalimentación casi instantánea sobre el estado de salud del código.

Técnicamente, la manera más sencilla de describir cómo hacer TDD es:

* Red-Green-Refactor-Repeat: Escribe una prueba que falla (red), escribe el código suficiente para que la prueba sea exitosa (green), después refactoriza para tener un buen diseño (también conocido como código simple) y por último repite.

Aunque a primer vistazo TDD aparenta ser muy sencillo requiere ciertas habilidades en tres principales áreas:

* Escribir pruebas
* Escribir código simple
* Refactorización

Y hay más, la Refactorización requiere:

* Reconocer síntomas de mal diseño (code smells)
* Hacer pequeñas transformaciones para mejorar el diseño. (Las refactorizaciones en curso)
* Reconocer un buen diseño (código simple)

## Diseño simple

Kent Beck, el creador de TDD, ha definido 4 reglas lograr tener un diseño simple:

* Ejecutar todas las pruebas.
* No tener código duplicado.
* Expresa las intenciones del desarrollador.
* Minimiza el número de clases y métodos.

## Libros recomendados

[Test-Driven Development by Example](https://www.amazon.com.mx/Test-Driven-Development-Example-Kent-Beck/dp/0321146530/ref=sr_1_1?ie=UTF8&qid=1510071240&sr=8-1&keywords=test+driven+development), Kent Beck

[Growing object oriented software, guided by tests](https://www.amazon.com.mx/Growing-Object-Oriented-Software-Guided-Tests/dp/0321503627/ref=sr_1_1?s=books&ie=UTF8&qid=1510071295&sr=1-1&keywords=growing+object+oriented), Steeve Freeman