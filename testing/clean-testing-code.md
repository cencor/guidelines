# Código de pruebas limpio

Así como generar o escribir código de producción, tener código de pruebas bueno y limpio requiere de mucho cuidado. Aquí enunciamos algunos tips para que obtengamos código de pruebas con bajos costos de mantenimiento que debes saber antes de seguir adelante y reducir el valor que nos pueden generar las pruebas automatizadas:

1. El código de pruebas es tan importante como el código de producción. Dale el mismo nivel de atención y cuidado. "Esto solo es código de pruebas" no es una excusa válida para justificar código descuidado.
2. Prueba una condición por cada prueba. Esto nos ayuda a mantener nuestras pruebas cortas y fáciles de entender.
3. Siempre recuerda la frase "Given, when, then" para que logremos mantener nuestras pruebas bien estructuradas.
4. La legibilidad importa mucho. No tratemos de sobre explotar la frase no reinventar la rueda o en inglés Don't Repeat Yourself (DRY). La duplicación no es mala siempre y cuando mejore la legibilidad. Encuentra un balance entre no duplicar código y legibilidad.

Adicionalmente puedes aplicar las mismas [reglas de diseño de Kent Beck](https://martinfowler.com/bliki/BeckDesignRules.html) para tu código de pruebas:

* Passes the tests
* Reveals intention
* No duplication
* Fewest elements