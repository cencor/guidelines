# Pruebas de UI

Muchas de las aplicaciones tienen de una u otra forma una interface de usuario. Típicamente estamos hablando de las interfaces web en el contexto de las aplicaciones web. También hay que recordar y no olvidar que una API REST o una interface de línea de comandos son tan interfaces de usuario como una deslumbrante interface de usuario.

Las pruebas de UI verifican que la interface de usuario de tu aplicación funciona correctamente. Los datos de entrada del usuario disparan las acciones correctas, los datos que deben ser presentados al usuario, el estado de la UI cambia como se espera.

Por otro lado en algunas ocasiones se piensa que las pruebas de UI y las pruebas end-to-end (punta-a-punta) son lo mismo. En estricto sentido cada una de ellas es diferente. Por ejemplo, ejecutar pruebas end-to-end en muchos casos significa hacerlo a través de la UI. Sin embargo no es verdad si invertimos la oración, ejecutar pruebas de UI no significa hacer pruebas end-to-end.

Probar la interface de usuario no tiene que ser realizado de una forma de prueba end-to-end. Dependiendo de la tecnología que estés utilizando, probar la interface de usuario puede ser tan simple como escribir algunas pruebas unitarias para tu código de Javascript en tu front-end generando stubs para el back-end.

Con aplicaciones web tradicionales probar la interface de usuario se puede lograr con herramientas como Selenium. Sin embargo si consideramos una API REST como nuestra interface de usuario, nosotros debemos tener todo lo que necesitamos al escribir pruebas de integración apropiadas alrededor de tu API.

Para las interfaces web hay muchos aspectos que probablemente queremos probar alrededor de nuestra UI: Comportamiento, layout, usabilidad o apego al diseño corporativo son solo algunas cuantas.

Afortunadamente, probar el comportamiento de nuestra interface de usuario es demasiado simple. Das un click aquí, ingresas datos allá y con eso deseamos que el estado de la interface cambie apropiadamente. Los frameworks modernos de single page app (como react, vue.js, Angular entre otros) regularmente vienen con sus propias herramientas que nos permiten probar todas estas interacciones a un nivel bajo (pruebas unitarias). Inclusive si nuestra implementación de front-end utiliza vanilla Javascript podemos utilizar herramientas de pruebas como Jasmin o Mocha. Con aplicaciones aún más tradicionales, aplicaciones que son rendereadas desde el servidor, las pruebas con Selenium serán tu mejor opción.

Por otro lado, verificar que el layout de tu aplicación web permanece intacto es mucho más difícil. Dependiendo de tu aplicación y las necesidades de tus usuarios tú podrás querer asegurar que los cambios en código no hagan dejar de funcionar el layout de tu sitio por accidente. El problema con este tipo de verificaciones es que las computadoras son notablemente malas para verificar si algo se ve bien. Quizás en el futuro exista algún algoritmo de machine learning capaz de hacerlo, mientras, no.

A pesar de estas limitantes hay algunas herramientas que nos pueden ayudar. Muchas de estas aplicaciones utilizan Selenium para abrir nuestra aplicación en distintos formatos y navegadores, toman capturas de pantallas y las comparan con las tomadas previamente. Si la anterior y la nueva difieren de una forma inesperada, la herramienta nos lo hará saber. Galen es una de estas herramientas así como lineup y jlineup.

Hemos ya mencionado como probar el estado de la UI así como el layout. Sin embargo una vez que quieras probar usabilidad y cosas "que se vean bonitas" necesitarás echar a un lado la automatización de pruebas. Esta es el área donde debes de confiar en pruebas de exploración, pruebas de usabilidad y demostraciones con tus usuarios para ver si les gusta tu producto y pueden usar todas las funcionalidades sin frustración, enojo o disgusto.