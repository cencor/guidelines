# Rest contract testing

Hemos visto como probar el contrato entre nuestro servicio y el servicio del notificador. Con esta interface nuestro servicio actúa como un consumidor, el notificador actúa como un proveedor. Ahora si pensamos en nuestro servicio nos daremos cuenta que nuestro servicio también actúa como un proveedor para otros: Nosotros proveemos una API REST que ofrece una operación lista para ser consumida por otros equipos.

Como hemos aprendido las pruebas de contrato son muy necesarias, nosotros por supuesto escribiremos una prueba de contrato para este contrato. En este caso vamos a asumir que este API es público. Esto significa que no necesariamente tenemos contratos dirigidos por consumidores (CDC). Aquí tenemos un par de opciones, generar nosotros un cliente de nuestro servicio, generar nuestro archivo Pact e implementar nuestra prueba de contrato como lo hizo el equipo del notificador o utilizar las herramientas que nos ofrece Spring directamente para validar nuestro contrato.

Con fines ilustrativos, utilizaremos la segunda opción para poder brindar una segunda alternativa de pruebas de contrato. Implementar la prueba de proveedor sigue el mismo patrón que hemos venido utilizado en todas las pruebas anteriores:

```
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class GreetingContractTest {

    /***/
    @Autowired
    private TestRestTemplate restTemplate;
 
    /***/
    @Test
    public final void greeting() {
        Greeting greeting = this.restTemplate.getForObject("/greeting/lastname/Wazowski", Greeting.class);
  
        Assert.assertThat(greeting.getId(), Matchers.greaterThan(0L));
        Assert.assertEquals("Hello Mike Wazowski!", greeting.getContent());
    }
 
    @Test
    public final void unknownContext() {
    ResponseEntity<Greeting> response = this.restTemplate.getForEntity("/noexiste", Greeting.class);
    
    Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}
```

La prueba GreetingContractTest utiliza la infraestructura de pruebas de Spring mediante la anotación @SpringBootTest donde le indicamos que levante nuestra aplicación en un puerto aleatorio y a cambio nos ofrece una plantilla TestRestTemplate que ya se encuentra configurada para ejecutar peticiones HTTP hacia nuestro servicio. Una vez que ejecutamos nuestra prueba invocamos nuestro endpoint y validamos los resultados.

En esta prueba debemos ser especialmente cuidadosos durante su mantenimiento. En la prueba resaltamos varios aspectos que son parte de nuestro contrato como el contexto de la URL: /greeting/lasname/{lastname} y la respuesta que devolvemos en este caso la clase Greeting que resulta de la conversión del JSON de respuesta que enviamos hacia este DTO. Esto significa que tanto la clase Greeting como el contexto de la URL de la prueba no deberían cambiar a lo largo del tiempo. De esta forma estaremos respetando el contrato que hemos expuesto. Hemos descubierto entonces que tenemos una ligera ventaja con Pact. De cualquier manera la importancia de tener pruebas de contrato en nuestros proyectos es sumamente alta. Depende de ti y las circunstancias de tu proyecto que opción elegir para realizar nuestra prueba de contrato.