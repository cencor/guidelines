# Mantén la estructura Given, When, Then.

Todas las pruebas deben contar con la estructura Given, When, Then. Es decir una o algunas precondiciones, **una sola acción** a probar, una o algunas verificaciones (`Assert`).

Si te es dificil detectar esta estructura en la prueba, es muy probable que tengas muchas precondiciones, muchas verificaciones o peor aún, muchas acciones. Si tuvieras muchas acciones, recuerda que una prueba solo ejecuta una acción. Por lo tanto es sinónimo de que estás haciendo más de una prueba. En caso de tener muchas precondiciones o muchas validaciones verifica que el código de producción a probar tenga solo una responsabilidad y un buen diseño. También revisa si estás duplicando algunas verificaciones que ya haces en otra prueba.

Quizás solo sea que necesites dar un poco de formato al código de la prueba. En estos casos separa con doble salto de línea entre cada parte de la prueba: Given, When, Then.

