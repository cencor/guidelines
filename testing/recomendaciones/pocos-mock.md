# Trata de usar pocos `mock`

Si para poder llevar a cabo la prueba requieres generar muchos __mock__, es muy probable que tengas algún problema de diseño en el código de producción. Quizás tengas muchas responsabilidades en la clase o método que estás evaluando. Échale un vistazo al diseño del código de producción por si encuentras más de una responsabilidad en él y esto ocasione que tengas que generar muchos __mock__
