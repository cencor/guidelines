# Limita el alcance de las pruebas de contrato

En ocasiones al estar elaborando pruebas de contrato es muy tentador hacerlo levantando toda la infraestructura y dependencias necesarias. De esta forma podemos garantizar que un recurso `REST` recibe y nos devuelve el contrato esperado, ¿cierto?.

Cuando estamos evaluando el contrato de un recurso `REST` de tipo `GET` pareciera que no hay ningún problema pues solo hacemos una consulta. 

Llevar a cabo una prueba de contrato a un recurso `REST` de tipo `POST`, `PUT` o `DELETE` puede traer efectos secundarios al resto de nuestras pruebas. La consecuencia es que al ejecutar una prueba de contrato de este tipo, estamos afectando el estado inicial de la base de datos en memoria. Esto es porque al ser una prueba de contrato, el código se ejecuta en un hilo independiente al de la prueba `@Test` que estamos ejecutando. Al ejecutarse en un hilo independiente **no** se puede llevar a cabo un `rollback` que deshaga los cambios realizados por nuestra operación `POST`, `PUT` o `DELETE`. 

Una vez que nuestros datos de prueba han quedado sucios, es posible que las siguientes pruebas fallen. Después evaluarás qué puede estar fallando en aquellas pruebas. Las ejecutas de forma aislada y son exitosas. Te quiebras la cabeza pensando en qué pudo haber ocasionado que fallaran. Tienes un universo de pruebas importante, digamos más de 30. Ignoras las 30 y vas ejecutando y descomentando una por una. Así hasta que te das cuenta que la prueba falla cuando se ejecuta la prueba de contrato. ¡Suspiras!

## ¿Cómo hago entonces para poder verificar el contrato?

En este tipo de pruebas lo recomendable es __mockear__ las dependencias del `@RestController`. No te preocupes que tienes otro conjunto de pruebas que validan que las dependencias estén bien configuradas. ¡Si no las tienes es el mejor momento para que las hagas! Recuerda que tendrías un montón de pruebas unitarias y algunas pruebas de integración responsables de eso.

A continuación una prueba de contrato donde se __mockean__ las dependencias de un `@RestController` y así poder hacer una prueba de contrato:

```
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class OrderBookRestControllerContractTest {

    /** El puerto donde se levanta el controller. */
    @LocalServerPort
    private int port;
    /** El template REST para llevar a cabo la prueba. */
    @Autowired
    private TestRestTemplate restTemplate;
    /** 
      * El servicio mockeado del que depende el controller. 
      * Con la anotación @MockBean le indicamos a Spring que inyecte 
      * este mock en vez del real. 
      */
    @MockBean
    private OrderBookService service;
    
    @Test
    void getPipReport() throws IOException {
        OrderBook orderbook = new PipOrderBook(new HashMap<>());
        orderbook.getRecords().addAll(orderBookRecordsSample());
        Mockito.when(service.getPipOrderBook(localDate, noon)).thenReturn(orderbook);
        
        ResponseEntity<Resource> response = restTemplate.exchange(
                "http://localhost:" + port + "/orderbook/report/provider/pip/date/2020-06-25/time/12:00",
                HttpMethod.GET, null, Resource.class);

        
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals("20200625120000_Corros.txt", response.getHeaders().getContentDisposition().getFilename());
        String report = new String(response.getBody().getInputStream().readAllBytes());
        String expectedReport = new String(
                Files.readAllBytes(Paths.get("src/test/resources/report/20200625130203_Corros.txt")));
        
        Assert.assertEquals(expectedReport, report);
    }
}
```