# Cantidad de verificaciones apropiada (`Assert`)

Las pruebas te pueden decir que tienes muchas validaciones (`Assert`). Tener muchos `Assert` es un reflejo de que la prueba tiene un alcance mayor al esperado. En estos casos escucha a la prueba y verifica si puedes generar otro tipo de prueba que te ayude a verificar algunas partes.

Si ya cuentas con alguna otra prueba que lleve a cabo ciertas verificaciones, no repitas entonces esas verificaciones.

