# Pruebas de exploración

A pesar de los cuidadosos y exhaustivos esfuerzos por automatizar pruebas, éstos no son perfectos. En ocasiones puedes olvidar algunas condiciones de frontera en tus pruebas de automatización. En algunas otras ocasiones es casi imposible detectar un bug en particular al generar una prueba unitaria. Algunos incidentes de calidad no se pueden verificar con tus pruebas automatizadas (por ejemplo, piensa en diseño o usabilidad). A pesar de tener las mejores intenciones con respecto a la automatización de pruebas, las pruebas manuales de algunas partes son una buena idea.

Incluyamos pruebas de exploración en nuestro portafolio de pruebas. El enfoque de pruebas manuales es el que brinda libertad y creatividad para detectar incidentes de calidad en un sistema en ejecución. Simplemente se necesita algo de tiempo, arremangar la camisa e intentar hacer fallar la aplicación. Utilizar una mentalidad destructiva y pensar en formas que puedan provocar incidentes y errores en la aplicación. Hay que buscar bugs, problemas de diseño, tiempos de respuesta lentos, mensajes de error faltantes, mensajes de error (para el usuario) que no ayudan a ser corregidos y todo aquello que te haría enojar o frustrar como usuario de tu propio software.

Una buena noticia es que podemos felizmente automatizar muchos de nuestros hallazgos con pruebas automatizadas, ya sea unitarias, de integración, de contrato, etc. Generar estas pruebas automatizadas para los bugs hallados nos asegura que el bug no volverá a aparecer en el futuro. Además nos ayuda a acortar la causa raíz de ese incidente durante la corrección del mismo. Por estas razones es muy importante que de todo lo que se descubra en las pruebas de exploración podamos automatizar la mayor parte. Recordemos que **no todo se puede automatizar**, nuevamente hacemos referencia a **temas de diseño o usabilidad**.

Durante las pruebas de exploración hallarás problemas que se filtraron por tu pipeline de construcción de manera inadvertida. No hay que frustrarse. Al contrario es una gran retroalimentación sobre la madurez de tu pipeline. Como con cualquier retroalimentación, asegura que generas acciones: 

* Piensa en qué puedes hacer para evitar este tipo de problemas en el futuro. 
* Quizás estén haciendo falta algún tipo de pruebas automatizadas. 
* Quizás hemos sido poco rigurosos con las pruebas automatizadas en esta iteración y necesitamos ser más integrales o exigentes con las pruebas automatizadas en el futuro. 
* Quizás existe alguna herramienta o enfoque que podamos incluir y usar en nuestro pipeline para evitar este tipo de cosas en el futuro. 

Lo más importante es que debemos asegurar que generemos una acción sobre la retroalimentación, sobre cada bug encontrado, en nuestro pipeline. De esta forma toda nuestra cadena de entrega de software irá creciendo con más madurez mientras sigamos agregando funcionalidad a nuestro software.