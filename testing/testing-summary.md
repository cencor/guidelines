# Conclusión

¡Finalmente llegamos a la conclusión! Esta sección es muy grande y una difícil lectura para explicar por qué y cómo debemos probar nuestro software. La excelente noticia es que esta información no caduca y es independiente a qué tipo de software estás haciendo. No importa si estás trabajando con un estilo de microservicios, dispositivos de Internet de las cosas (IoT), aplicaciones móviles o aplicaciones web, las lecciones aprendidas en esta sección pueden ser aplicadas a todas estas aplicaciones.

Esperando que sea de mucha utilidad esta sección ahora vamos a revisar el código de ejemplo y que muchos de los conceptos explicados en esta sección se vean reflejados en tu portafolio de pruebas que estarás generando de ahora en adelante. Tener un portafolio sólido de pruebas requiere de un gran esfuerzo. Este traerá muchos frutos en el largo plazo y hará nuestra vida como desarrolladores más pacífica. ¡Confía y verifícalo con tus propias pruebas!

Por último podemos proponer una pirámide de pruebas como la siguiente donde ya tenemos bien claro que y como automatizar pruebas en cada una de los distintos niveles:

![Pirámide de pruebas](testingPyramid.png)

## ¿Interesado en leer más?

[https://martinfowler.com/articles/mocksArentStubs.html](https://martinfowler.com/articles/mocksArentStubs.html)
[https://martinfowler.com/articles/microservice-testing/](https://martinfowler.com/articles/microservice-testing/)
[Test Driven Development - Kent Beck](https://www.amazon.com.mx/Test-Driven-Development-Kent-Beck/dp/0321146530/ref=sr_1_2?keywords=test+driven+development&qid=1659715624&sprefix=test+driven+de%2Caps%2C121&sr=8-2)
[Growing Object Oriented Software Guided by Tests - Steve Freeman](https://www.amazon.com.mx/Growing-Object-Oriented-Software-Guided-Tests/dp/0321503627/ref=sr_1_1?__mk_es_MX=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3QL2WMOYHJOSN&keywords=Growing+Object+Oriented+Software+Guided+by+Tests&qid=1659715639&sprefix=growing+object+oriented+software+guided+by+tests%2Caps%2C190&sr=8-1)
[Implementing Domain-Driven Desing - Vaughn Vernon](https://www.amazon.com.mx/Implementing-Domain-Driven-Design-Vaughn-Vernon/dp/0321834577/ref=sr_1_1?keywords=implementing+domain+driven+design&qid=1659715743&sprefix=Implementing+%2Caps%2C115&sr=8-1)
[Continuos Delivery - Jez Humble](https://www.amazon.com.mx/Continuous-Delivery-Reliable-Deployment-Automation/dp/0321601912/ref=sr_1_1?__mk_es_MX=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=374H8OVGBTTIE&keywords=continuous+delivery&qid=1659715652&sprefix=continuos+delivery%2Caps%2C195&sr=8-1)
[Microservices Patterns - Chris Richardson](https://www.amazon.com.mx/Microservices-Patterns-Examples-Chris-Richardson/dp/1617294543/ref=sr_1_1?keywords=microservices+patterns&qid=1659715683&sprefix=microservices+pa%2Caps%2C113&sr=8-1)
[The Phoenix Project - Gene Kim](https://www.amazon.com.mx/Phoenix-Project-Helping-Business-English-ebook/dp/B078Y98RG8/ref=sr_1_1?__mk_es_MX=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=31J2IACKJXDYD&keywords=The+Phoenix+Project&qid=1659715718&s=amazon-super&sprefix=the+phoenix+project%2Cspecialty-aps%2C202&sr=8-1-catcorr&srs=18073069011&ufe=app_do%3Aamzn1.fos.649fe0ca-1cbf-43f1-a365-f15699263f39)