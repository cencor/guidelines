# Pruebas unitarias

La base de tu conjunto de pruebas será sustentada por pruebas unitarias. Tus pruebas unitarias aseguran que una cierta unidad (tu sujeto de prueba) de tu base de código funciona como se espera. Las pruebas unitarias cuentan con el alcance más corto de todas tus pruebas en tu conjunto de pruebas. El número de pruebas unitarias en tu conjunto de pruebas sobrepasará y por mucho cualquier otro tipo de prueba.

## ¿Qué es una unidad?

Si preguntamos a tres distintas personas qué significa unidad en el contexto de pruebas unitarias, probablemente obtendremos cuatro o cinco respuestas diferentes. Hasta cierto punto es un asunto de tu propia definición y está bien no tener una definición canónica o de diccionario.

Si estamos trabajando con un lenguaje funcional, una unidad es muy probable que sea una simple función. Tus pruebas unitarias llamarán esa función con diferentes parámetros y asegurarán que devuelve los resultados esperados. En un lenguaje orientado a objetos una unidad puede variar desde un simple método hasta una clase completa.

## Sociable y solitario

Algunos argumentan que todos los colaboradores (por ejemplo otras clases que invoca tu clase bajo prueba) de tu sujeto de prueba deben ser sustituidos por mocks o stubs para lograr tener un aislamiento total y así evitar efectos secundarios y una configuración muy complicada de la prueba. Otros argumentan que solo los colaboradores que son lentos o tienen efectos secundarios más grandes (por ejemplo clases que acceden bases de datos o hacer invocaciones a través de la red) deben ser **_stubeados_** o **_mockeados_**.

En algunos lugares se identifican estos tipos de pruebas como las **pruebas unitarias solitarias** para las pruebas que stubean (simulan) todos los colaboradores y **pruebas unitarias sociables** para las pruebas que permiten interactuar con colaboradores reales. Jay Fields fue quien originó estos términos y los puedes consultar en su libro Working effectively with Unit Tests. Si tienes algún tiempo libre adicional en aquellas tardes de lluvia puedes leer más sobre las pros y contras de stubs y mocks en el artículo [Mocks aren't stubs de Martin Fowler](https://martinfowler.com/articles/mocksArentStubs.html).

Al final de todo no importa tanto si decides por pruebas solitarias o sociales. Generar pruebas automatizadas es lo que realmente importa. Personalmente yo utilizo ambos esquemas todo el tiempo. Si es un poco incómodo utilizar colaboradores reales entonces uso mocks y stubs generosamente. Si siento que involucrar un colaborador real me da mas confianza en mi prueba simplemente simulo (stub o mock) las partes más externas de mi servicio, aquellas que no me dan tanto valor.

## Mocking y Stubing

Mocks y Stubs son dos tipos diferentes de dobles de pruebas. Aunque hay más de dos tipos de dobles de pruebas nos enfocaremos en estos dos. En muchas ocasiones los términos Mock y Stub se utilizan sin diferencia como si fuera lo mismo. Es bueno precisar y mantener sus propiedades y beneficios específicos en nuestras mentes. Aunque no debemos olvidar que siguen siendo dobles de pruebas que podemos utilizar para reemplazar objetos que usaríamos en producción (o algún otro ambiente preproductivo) con una implementación que nos pueda ayudar en nuestras pruebas.

En simples palabras significa que podemos remplazar algo real, por ejemplo una clase, módulo o función, con una versión "falsa" de ese algo. La versión "falsa" se ve y actúa como la real (responde a las mismas invocaciones) pero responde con resultados preestablecidos que tu defines al inicio de tu prueba.

El utilizar dobles de pruebas no es específico de pruebas unitarias. Algunos dobles más elaborados pueden ser usados para simular partes completas de tu sistema de manera controlada. Aunque en pruebas unitarias es mucho más probable encontrar muchos, muchos, mocks y stubs (dependiendo si eres sociable o solitario), simplemente porque en muchos de los lenguajes modernos y librerías es muy fácil y cómodo configurar mocks y stubs.

Sin importar la tecnología de tu elección, existe una alta probabilidad de que ya sea las librerías estándar del lenguaje o algún framework popular de un tercero te ayude con formas elegantes de configurar mocks. Inclusive escribir tus propios mocks desde cero es solo cuestión de escribir una clase, módulo o función con la misma firma (contrato) del real y configurar la respuesta en tu prueba.

Tus pruebas unitarias se ejecutarán muy rápido. En una máquina decente podrías esperar ejecutar miles de pruebas unitarias en unos pocos minutos, generalmente menos de 5 minutos. Prueba pequeñas partes de tu base de código de manera aislada y evitando llegar a las bases de datos, filesystems o ejecutar consultas por HTTP (utilizando mocks y stubs para estas partes) para mantener la ejecución de tus pruebas rápida. No sólo la velocidad es importante, es aún más importante que las pruebas unitarias sean predecibles y tener el control de los datos de prueba. Por ejemplo al ejecutar pruebas unitarias contra una base de datos real, los datos de los que dependemos para la ejecución de nuestra prueba pudieron haber sido modificados por alguien más, lo que ocasionaría que nuestra prueba falle y que sería muy costoso diagnosticar que la falla fue por datos y no por modificación del código.

Una vez que te pongas a escribir pruebas unitarias te convertirás más rápido y ágil para escribirlas. Simula colaboradores externos, configura datos de entrada, invoca tu sujeto de prueba y verifica que el valor retornado es lo que estabas esperando. Échale un ojo a [Test-Driven Development](https://en.wikipedia.org/wiki/Test-driven_development) y deja que tus pruebas unitarias guíen tu desarrollo. Si TDD se aplica correctamente te puede ayudar bastante a tener un buen diseño al mismo tiempo que sea mantenible mientras que vas produciendo un conjunto de pruebas que cubre todos los casos y completamente automatizado. Aun así esto no es la bala de plata. Adelante, dale una oportunidad real y ve que se siente.

## ¿Qué probar?

Algo muy bueno de las pruebas unitarias es que tú puedes escribir pruebas para todas tus clases de código de producción, sin importar su funcionalidad o a que capa de tu estructura interna pertenecen. Puedes probar de manera unitaria controllers así como repositorios, clases de dominio o lectores de archivos. Simplemente apégate a la regla de **una clase de prueba por clase de producción** y estás listo para empezar.

Una clase de prueba unitaria debe al menos **probar la interface pública de la clase**. Los métodos privados no pueden ser probados pues simplemente no los puedes invocar desde la clase de pruebas. Los métodos protegidos o protegidos a nivel de paquete son accesibles desde tu clase de prueba (siempre y cuando tu clase de prueba cuente con la misma estructura de paquete que la clase de producción).

Existe una delgada línea cuando se trata de escribir pruebas unitarias: Estas deben asegurar que todos los caminos son probados (incluyendo happy path y condiciones de frontera). Al mismo tiempo que no deben estar tan atadas a la implementación.

### ¿Por qué?

Las pruebas que están completamente atadas al código de producción rápidamente se vuelven un dolor de cabeza. Tan pronto realices una refactorización de tu código de producción (rápidamente, refactorizar significa cambiar la estructura interna de tu código sin cambiar el comportamiento externo) tus pruebas unitarias dejarán de funcionar.

De este modo vas a perder uno de los más grandes beneficios de las pruebas unitarias: ser una red de cirquero (seguridad) para los cambios de código. En vez de eso te hartarás de aquellas pruebas frágiles que fallan cada vez que realizas una refactorización, lo que genera más trabajo en vez de ayudarte y te puede llevar a preguntarte quién fue aquél que se le ocurrió este tipo de pruebas inútiles.

¿Qué puedes hacer para no caer en ese anti patron? No reflejes tu estructura de código interno en tus pruebas unitarias. Mejor prueba para observar el comportamiento. Piensa lo siguiente:

* Si yo ingreso los valores X y Y, ¿el resultado será Z?

En vez de:

* Si yo ingreso X y Y, el método invocará la clase A primero, después la clase B y finalmente ¿Me regresará el resultado de la clase A más el resultado de la clase B?

Los métodos privados deben ser considerados como detalle de implementación. Ese es el verdadero por qué no deberías intentar probarlos directamente.

Existen muchas críticas a pruebas unitarias y TDD argumentando que escribir pruebas unitarias es un trabajo sin sentido donde tienes que probar todos tus métodos para que puedas tener un alto porcentaje de cobertura de pruebas. Quienes generan este tipo de críticas citan escenarios donde un equipo demasiado ansioso o su jefe los obliga a escribir pruebas unitarias para getters y setters y cualquier otro código trivial para poder lograr un 100% de cobertura de pruebas.

Y están totalmente equivocados con eso.

Sí, debes de probar la interface pública. Aún más importante, **no vas a probar código trivial**. No vas a ganar nada probando simples getters o setters o alguna implementación trivial (por ejemplo, aquella que no tenga lógica condicional). Puedes aprovechar que ese tipo de código será probado **indirectamente** por las pruebas que realmente importan. Además puedes sacar provecho de la cobertura de aquellos getters y setters que no se utilizan, eliminándolos de tu código.
