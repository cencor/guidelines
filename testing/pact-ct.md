# Equipo consumidor (Nuestro equipo)

Supongamos que nuestro microservicio va a consumir un nuevo API. El API del notificador, un componente responsable de envío de emails. Así que es nuestra responsabilidad escribir una prueba de consumidor que defina nuestras expectativas para el contrato (el API) entre nuestro microservicio y el servicio de notificación.

Primero incluimos la librería para escribir pruebas de consumidor con Pact en nuestro build.gradle

```
testCompile group: 'au.com.dius', name: 'pact-jvm-consumer-junit_2.12', version: '3.5.15'
```

Gracias a esta librería podemos implementar una prueba de consumidor y utilizar los servicios para generar mocks de Pact:

```
@RunWith(SpringRunner.class)
@SpringBootTest
public class NotifierClientConsumerTest {
    
    @Autowired
    private RestEmailer emailer;
    @Value("${notifier.service.apikey}") 
    private String apikey;
    @Rule
    public PactProviderRuleMk2 notifierProvider = new PactProviderRuleMk2("emailNotifier", 
            "localhost", 8089, this);

    @Pact(consumer = "test_consumer")
    public RequestResponsePact createPact(PactDslWithProvider builder) throws IOException, URISyntaxException {
        String validRequestBody = new String(Files.readAllBytes(Paths.get(
                ClassLoader.getSystemResource("validEmail.json").toURI())));

        String expectedJsonResponse = new String(Files.readAllBytes(Paths.get(
                ClassLoader.getSystemResource("validEmailResponse.json").toURI())));
        return builder
                .given("")
                .uponReceiving("Una solicitud de envio de correo electronico")
                .path("/notifier/notification/mail")
                .headers("apiKey", apikey)
                .body(validRequestBody, ContentType.APPLICATION_JSON)
                .method("POST")
                .willRespondWith()
                .status(202)
                .body(expectedJsonResponse, ContentType.APPLICATION_JSON)
                .toPact();
    }
    
    @Test
    @PactVerification("emailNotifier")
    public void shouldSendAnEmail() {
        String correlationId = emailer.send(
                "mike.wazowski@monstersinc.com", 
                "Ordena tu papeleo", 
                "No ordenaste tu papeleo anoche!");
        Assert.assertEquals("1", correlationId);
    }
}
```

Si miramos con detalle, veremos que NotifierClientConsumerTest es muy similar a nuestra prueba de integración RestEmailerTest. En vez de utilizar Wiremock como servidor stub en esta ocasión utilizamos Pact. De hecho esta prueba de consumidor funciona de la misma forma que la prueba de integración, reemplazamos el servicio de un tercero con un stub, definimos la respuesta esperada y verificamos que nuestro cliente puede parsear correctamente la respuesta. En estricto sentido la prueba NotifierClientConsumerTest es una prueba de integración.

La ventaja sobre las pruebas basadas en Wiremock es que esta prueba genera un archivo pact (Encontrado en target/pacts/<nombreDelPact>.json) cada vez que la prueba corre. Este archivo pact describe nuestras expectativas del contrato en un formato JSON. Este archivo puede ser utilizado para verificar que nuestro servidor stub se comporta como el servidor real. Podemos tomar el archivo pact y entregarlo al equipo que provee el servicio. Ellos toman este archivo pact y escriben una prueba de proveedor utilizando las expectativas definidas en el archivo. De esta forma ellos prueban si su API cumple con nuestras expectativas. A continuación observamos el json generado por nuestra prueba de consumidor:

```
{
    "provider": {
        "name": "emailNotifier"
    },
    "consumer": {
        "name": "test_consumer"
    },
    "interactions": [
        {
            "description": "Una solicitud de envio de correo electronico",
            "request": {
                "method": "POST",
                "path": "/notifier/notification/mail",
                "headers": {
                    "apiKey": "l7xx0f5696cfaccb4892bcd603623db9affd",
                    "Content-Type": "application/json; charset=UTF-8"
                },
                "body": {
                    //JSON no soporta comentarios pero se coloca intencionalmente aqui
                    //Para mostrar el contenido de validEmail.json, que el contrato propuesto
                    //por el cliente, que incluye unicamente lo que necesita enviar.
                    "to": [
                        "mike.wazowski@monstersinc.com"
                    ],
                    "body": "No ordenaste tu papeleo anoche!",
                    "subject": "Ordena tu papeleo"
                }
            },
            "response": {
                "status": 202,
                "headers": {
                    "Content-Type": "application/json; charset=UTF-8"
                },
                "body": {
                    "correlationId": "1"
                }
            },
            "providerStates": [
                {
                    "name": ""
                }
            ]
        }
    ],
    "metadata": {
        "pact-specification": {
            "version": "3.0.0"
        },
        "pact-jvm": {
            "version": "3.5.15"
        }
    }
}
```

Podrás observar que aquí es donde viene la parte consumer-driven de CDC. El consumidor dirige la implementación de la interface al describir sus expectativas y listo. No hay sobreingeniería, no más cosas que no necesitas.

Hacerle llegar el archivo pact al equipo proveedor del servicio puede suceder de distintas formas. La más simple es agregar el archivo pact a nuestro control de versiones y pedirle al otro equipo que siempre obtenga la versión más actualizada del archivo pact. Una más avanzada es utilizar el repositorio de artefactos o un servicio como el de Amazon S3.

En tu aplicación real no necesitarás ambas, la prueba de integración y la de consumidor para verificar tu clase cliente. En ese ejemplo se contienen ambos para mostrar como usar uno o el otro. El esfuerzo requerido para escribir las pruebas es el mismo. Pact tiene el beneficio de generar automáticamente un archivo pact con las expectativas del contrato que otro equipo puede usar fácilmente para implementar sus pruebas de proveedor. Por supuesto, esto solo tiene sentido si el otro equipo utilizará el contrato definido en el archivo pact. Definamos entonces que para desarrollos entre internos de CENCOR utilicemos Pact. Si el otro equipo es un proveedor externo, simplemente nuestro archivo pact no traerá beneficios. En estos casos prefiramos utilizar la combinación de pruebas de integración y Wiremock. De cualquier manera nos estamos apegando a un contrato.

# Equipo proveedor (el otro equipo)

El equipo proveedor es implementado por el equipo del API del notificador. Podemos resumir que hay dos tipos de proveedores: Proveedores que ofrecen APIs públicas y proveedores dentro de nuestra compañía.

Un proveedor de APIs públicas en teoría implementaría una prueba de proveedor de su lado para verificar que ellos no han violado el contrato definido entre su aplicación y los clientes. Evidentemente ellos no se preocupan por nuestra simple aplicación de demostración y no implementarán una prueba CDC para nosotros. Básicamente esta es la gran diferencia entre un API expuesta públicamente y una API privada dentro de una compañía adoptando microservicios. Las APIs expuestas públicamente no pueden considerar a todos y cada uno de sus clientes que hay en la calle. De hacerlo ellos se volverían incapaces de realizar movimientos.

Dentro de nuestra organización nosotros podemos y debemos. Nuestra aplicación servirá pocos servicios, quizás una docena como máximo. Estarás bien al escribir pruebas de proveedor para mantener un sistema estable.

El equipo proveedor obtiene el archivo pact y lo ejecuta contra su propio servicio. Al hacerlo están implementando una prueba de proveedor que lee el archivo pact, genera datos de prueba y ejecuta las expectativas definidas en el archivo pact contra su servicio.

Los amigos de Pact han escrito muchas librerías para implementar pruebas de proveedor. Puedes echar un lente en su [repositorio Github](https://github.com/pact-foundation/pact-jvm) para tener una buena introducción sobre las librerías de proveedor y consumidor disponibles. Escoge aquella que más se apega al stack tecnológico.

El API del notificador también está implementada con Spring boot. En este caso ellos utilizan el [proveedor de Pact de Spring](https://github.com/pact-foundation/pact-jvm/tree/master/pact-jvm-provider-spring) que encaja perfectamente con los mecanismos de mock de Spring. La implementación de la prueba de proveedor por parte del equipo del Notificador implementa su prueba de proveedor de la siguiente manera:

```
@RunWith(SpringRestPactRunner.class)
@Provider("emailNotifier") // El mismo que definimos en provider_name
@PactFolder("src/test/resources/pacts") // Indica donde cargar los archivos pact
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class EmailerContractTest {

    @TestTarget
    public final Target target = new SpringBootHttpTarget();

    @State("send email") // El mismo que el valor de given() en nuestra prueba de consumidor.
    public void sendEmail() {
        /*
         * En este proyecto se esta utilizando una base de datos en memoria y un
         * broker de mensajeria JMS (AMQ) en memoria durante pruebas.
         * 
         * Esto significa que tanto los datos de prueba como las dependencias de
         * otros servicios (JMS) se reemplazan por stubs durante la ejecucion de
         * pruebas. De esta forma no hay nada que ajustar en esta prueba. Todo
         * fluye de manera automatica como si estuviese el servicio conectado
         * con todas sus dependencias reales.
         * 
         * Podemos apreciar las ventajas que nos ofrece la inyección de
         * dependencias al poder sustituir elementos que nos harian muy
         * complicado ejecutar la prueba.
         * 
         * En resumen este componente valida el cuerpo recibido en la peticion,
         * genera un correlation Id e indica a otro componente, a traves de JMS,
         * que ejecute el envio del email de forma asincrona.
         * 
         * Al estar utilizando una base de datos en memoria tenemos el control
         * total de los datos. Por ejemplo del correlation Id que es una
         * secuencia de base de datos. Al ser una base de datos en memoria
         * aseguramos que cada vez que se ejecute la prueba la secuencia se
         * encuentra como nueva, por lo tanto nos devuelve 1, que es el valor
         * esperado en el archivo Pact.
         * 
         * Si hubiesen datos que no podemos empatar con nuestra infraestructura
         * de pruebas pudieramos sustituir algunas piezas de nuestro servicio
         * utilizando JMockit como lo realizamos en la prueba unitaria.
         */
    }
}
```

Esta prueba EmailerContractTest necesita proveer los estados definidos en el archivo pact que se nos proporcionó. Prácticamente uno por cada expectativa. En este ejercicio sólo definimos una por lo tanto sólo tenemos un estado. Una vez que ejecutamos esta prueba de proveedor, o de contrato, Pact leerá el archivo pact y generará una petición HTTP hacia nuestro servicio que después genera una respuesta de acuerdo al estado que se definió en el archivo pact. Veamos la salida de la ejecución de la prueba:

```
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v1.5.2.RELEASE)

Verifying a pact between test_consumer and emailNotifier
  Given send email
  Una solicitud de envio de correo electronico
    returns a response which
      has status code 202 (OK)
      includes headers
        "Content-Type" with value "application/json; charset=UTF-8" (OK)
      has a matching body (OK)
```

¿Cómo sabe Spring y/o Pact invocar nuestro servicio? Spring nos ofrece infraestructura de pruebas muy práctica. Al definir @SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT) estamos indicando que levante nuestra aplicación en un servidor HTTP con un puerto aleatorio. Posteriormente se le indica a Pact mediante

```
@TestTarget
public final Target target = new SpringBootHttpTarget();
```

que estamos utilizando el servidor HTTP proporcionado por Spring en un puerto aleatorio. Al instanciar SpringBootHttpTarget obtiene del contexto de Spring el puerto en el cual está ejecutándose nuestro servicio y listo. Al momento de verificar los métodos marcados con @State obtiene el path, body y demás parámetros de la petición a ejecutar para ese estado del archivo Pact. De esta forma es como se ejecutan las pruebas.

Como puedes ver, todo lo que tiene que hacer la prueba es cargar el archivo pact y después definir nuestros datos de prueba (en nuestro caso con una base de datos en memoria, en su defecto podemos usar mocks). No hay ninguna prueba personalizada que implementar. ¡Todo se deriva del archivo pact!