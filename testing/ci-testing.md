# Ejecuta las pruebas en el pipeline de CI

Nosotros que usamos integración continua (CI) y entregas continuas (CD) de forma regular, tenemos un pipeline que ejecuta nuestras pruebas automatizadas cada vez que ejecutamos un cambio en nuestro software. Usualmente estos pipelines se dividen en diferentes etapas que gradualmente nos darán más confianza y certidumbre de que nuestro software está listo para ser desplegado en producción.

Ahora, sabiendo que existen todas estas distintas pruebas, te estás preguntando cómo las colocamos en nuestro pipeline. Para responder esta inquietud sólo debes de pensar en uno de los valores fundamentales de Continuos Delivery, que de hecho es uno de los principales valores en eXtreme Programming y desarrollo de software ágil: **Fast Feedback.**

Un buen pipeline nos dice en qué fallamos lo más pronto posible. No queremos estar esperando una hora sólo para descubrir que nuestro último cambio descompuso algunas pruebas unitarias sencillas. En este esquema es muy probable que ya te hayas ido a casa si es que el pipeline toma mucho tiempo en darnos retroalimentación.

Lo mejor es que podemos obtener esta información en cuestión de segundos, quizás unos pocos minutos, al colocar las pruebas que se ejecutan muy rápido en las etapas más tempranas de nuestro pipeline. De forma inversa colocamos las pruebas más lentas, usualmente las que tienen un alcance más amplio, en las etapas finales con el fin de no detener el feedback o retroalimentación de las pruebas que se ejecutan rápidamente. Podemos decir que definir las etapas de nuestro pipeline de despliegue no es determinado por el tipo de pruebas, más bien por su velocidad y alcance. Con estas consideraciones es muy razonable poner algunas de las pruebas de integración de corto alcance y rápida ejecución en la misma etapa que nuestras pruebas unitarias, simplemente porque nos dan un feedback más rápidamente y no porque queremos pintar una línea formal entre los distintos tipos de prueba.

Para poder ilustrar o ejemplificar, podemos colocar nuestras pruebas unitarias, pruebas de integración con base de datos, pruebas de integración con otros servicios, pruebas de contrato a que sean ejecutadas por la herramienta de construcción gradle o maven y dejamos las pruebas de UI y End-to-End para etapas posteriores. Haciéndolo de esta manera podemos indicarle a nuestro pipeline que ejecute el siguiente comando:

```
gradle clean build
```

Este comando además de compilar nuestra aplicación buscará todas las pruebas realizadas con JUnit que se encuentren en el folder de pruebas `/src/test/java` y las ejecutará. En esta carpeta podemos colocar todas nuestras pruebas unitarias, de integración con BD, de integración con otros servicios y de contrato. En caso de que alguna de ellas falle la construcción se detiene y nos indica qué pruebas fallaron. Veamos la salida de la ejecución de la aplicación de ejemplo:

```
gradle clean build
:clean
:compileJava
:processResources UP-TO-DATE
:classes
:findMainClass
:jar
:bootRepackage
:assemble
:compileTestJava
:processTestResources
:testClasses
:test
2018-05-21 16:54:17.909  INFO 23324 --- [      Thread-20] ationConfigEmbeddedWebApplicationContext : Closing org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@2c65e3b4: startup date [Mon May 21 16:54:13 CDT 2018]; root of context hierarchy
:check
:build
BUILD SUCCESSFUL

Total time: 38.831 secs
```

¡En 39 segundos nos podemos dar cuenta si nuestra aplicación continua funcionando!

Lo mejor de esto es que nosotros mismos podemos accionar el mismo comando en nuestro equipo de desarrollo, sin siquiera tener que subir nuestros cambios. 

Adoptar este esquema nos hará aún más rápidos pues antes de subir nuestros cambios a Git, sabremos si hemos descompuesto algo. Entonces, siguiendo este esquema, el pipeline comienza a tener otra responsabilidad. Si nosotros aseguramos que en nuestro equipo de desarrollo el comando `gradle clean build` fue ejecutado exitosamente, y este mismo falla una vez que subimos nuestros cambios, podremos identificar más fácilmente la falla y se puede reducir a pocas razones: 

* No subimos todos nuestros cambios 
* Tenemos alguna prueba que está personalizada a nuestro equipo de desarrollo. Por ejemplo, una prueba que lea un archivo de una ruta de un equipo con windows y el pipeline se ejecuta sobre un equipo linux. 
* Tenemos alguna prueba frágil. Por ejemplo asociada a un timezone en particular

Al eliminar y estandarizar este tipo de cosas nuestro software y pipeline de despliegue comienzan a ser cada vez más maduros y confiables.