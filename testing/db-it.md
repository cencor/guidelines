# Integración con base de datos

La clase PersonRepository es la única clase de tipo repositorio en nuestro código. Está basada en Spring Daŧa y no tiene ninguna implementación. Simplemente extiende la interface CrudRepository y provee un par de métodos. El resto es la magia de Spring.

```
public interface PersonRepository extends CrudRepository<Person, Long> {
    /**
     * @param lastName El apellido de la persona a buscar.
     * @return Una lista de las personas con ese apellido.
     */
    Optional<Person> findByLastName(String lastName);

    /**
     * @param name El nombre de la persona a buscar
     * @param lastName El apellido de la persona a buscar.
     * @return Todas las personas que contengan el nombre y apellido proporcionado.
     */
    Optional<Person> findByNameAndLastName(String name, String lastName);
}
```

Con la interface CrudRepository Spring Boot ofrece un repositorio CRUD completamente funcional con los métodos findOne, findAll, save, update y delete. Nuestra definición personalizada del método findByLastName extiende la funcionalidad básica y nos da una manera de obtener Personas a partir de su apellido. Spring Data analiza el tipo de retorno del método, su nombre del método y verifica el nombre del método contra la convención de nombrado para darse cuenta que debe hacer.

A pesar de que Spring Data hace mucho del trabajo pesado de la implementación de este repositorio vamos a escribir una prueba de integración con base de datos. Podrás argumentar que esto es probar el framework. Aún así tener varias pruebas de integración es crucial. Primero verificaremos que nuestro método findByLastName realmente funciona y se comporta como esperamos. Segundo prueba que nuestro repositorio está cableado correctamente por Spring y nos podemos conectar a la base de datos. Tercero, nos ayuda a verificar que nuestra entidad Person esté mapeada correctamente con los campos y los nombres de la tabla real.

Es importante mencionar que al realizar nuestras pruebas contra una base de datos en memoria significa que estamos probando con una base de datos distinta a la de producción. Sin embargo realizar este tipo de pruebas reduce de manera importante algunos errores de integración, al mismo tiempo que mantenemos junto con el proyecto en control de versiones un esquema de nuestra base de datos.

Suficiente explicación, procedamos a la prueba de integración que guarda una persona en la base de datos y la encuentra por su apellido.

```
@RunWith(SpringRunner.class)
@SpringBootTest
/*
 * En una clase de pruebas la anotación @Transactional hará que cada uno de los
 * métodos se vuelva transaccional y al finalizar cada método de prueba se
 * realizará un Rollback por default con el fin de dejar intacta la base de
 * datos en memoria y pueda ser reutilizada por más pruebas.
 */
@Transactional
public class PersonRepositoryTest {

    @Autowired
    private PersonRepository repository;
    @Autowired
    private EntityManager entityManager;

    @Test
    public final void savePerson() {
        //Given
        Person randal = new Person("Randall", "Boggs");
        repository.save(randal);
        /*
         * Este metodo se encuentra dentro de una transacción. Para JPA
         * significa que no realizará la instrucción de INSERT si no hasta que
         * se de el COMMIT. Para el caso de pruebas todas las transacciones
         * realizan un rollback para dejar la base de datos en memoria en su
         * estado original y permitir que otras pruebas inicien con el mismo
         * estado. Por lo tanto nunca se ejecuta el COMMIT.
         * 
         * Entonces para poder verificar que el INSERT fue generado
         * correctamente y que la BD en memoria valide todos los constraints, es
         * necesario indicarle que realice la instrucción INSERT en este momento
         * con un flush.
         */
        entityManager.flush();


        //When
        Optional<Person> mayContainRandall = repository.findByLastName("Boggs");


        //Then
        Assert.assertTrue(mayContainRandall.isPresent());
        Assert.assertEquals(randal.getName(), mayContainRandall.get().getName());
        Assert.assertEquals(randal.getLastName(), mayContainRandall.get().getLastName());
    }
    
    @Test
    public final void fetchPerson() {
        //Given
        /*
         * En esta prueba los datos de prueba son generados en el archivo
         * data.sql que es cargado en la base de datos en memoria.
         */

        
        //When
        Optional<Person> mayContainMike = repository.findByLastName("Wazowski");

        
        //Then
        Assert.assertTrue(mayContainMike.isPresent());
        /*
         * Durante esta prueba se descubrió que los tipos de dato CHAR en base
         * de datos son de longitud fija. Esto significa que todos los datos
         * serán completados con espacios al final para completar el tamaño
         * especificado en la columna, en este caso de 25.
         * 
         * Durante la prueba se pudo descubrir este comportamiento y podemos
         * tomar algunas decisiones. Por ejemplo realizar el trim dentro de la
         * entidad Person al momento de realizar el getXxx() o bien realizar el
         * trim() una vez que se obtuvo el campo, como se realizó en esta
         * prueba.
         */
        Assert.assertEquals("Mike", mayContainMike.get().getName().trim());
        Assert.assertEquals("Wazowski", mayContainMike.get().getLastName().trim());
    }
}
```

Puedes observar que nuestra prueba de integración sigue la misma estructura de configurar datos de pruebas, ejecutar y evaluar como en las pruebas unitarias. ¡Este es un concepto universal!

## Configuración de la BD en memoria

La prueba de integración se ve bastante sencilla aunque hacen falta algunos otros componentes. Echemos un vistazo a la clase InMemoryDataConfig:

// Mostrar app.properties con liquibase

Dentro de esta clase de pruebas podemos observar que se está generando un DataSource que apunta a la BD en memoria de HSQL misma que recibe el script de creación de esquema más los datos iniciales. Echemos un vistazo a la creación del esquema:

// Mostrar xml de liquibase

En la definición de este script podemos decirle a HSQL que se comporte como una base de datos MySQL y el resto es básicamente la creación del esquema. Como puedes observar es básicamente el mismo script que se utiliza para crear la BD real. Es cierto, HSQL no es una base de datos completa por lo que algunas sentencias o cosas muy particulares de MySQL no están soportadas. Pero en general, las ventajas de poder realizar pruebas de integración con una BD en memoria sobrepasan este tipo de limitaciones.

La entidad Person donde mapeamos contra la tabla:

```
Aggregate
```

Finalmente un poco de configuración de Spring:

`src/test/resources/application.properties`
```
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5Dialect
spring.jpa.properties.hibernate.default_schema=PUBLIC
```

HSQL por default cuenta con el esquema PUBLIC y le indicamos que utilice el dialecto de MySQL. Para producción se cuenta con otro application.properties donde indicamos el esquema correcto.

En este ejemplo se está utilizando JPA para acceder a la Base de datos. Sin embargo si, por la razón que sea, es necesario que vayas con JDBC puro puedes basarte en los mismos conceptos y obtendrás el beneficio de verificar que tus consultas (Queries) estén implementados y mapeados correctamente tras ejecutar tus pruebas de integración con JUnit muy rápidamente.