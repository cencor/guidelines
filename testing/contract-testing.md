# Pruebas de contrato

Cada vez más y más organizaciones de desarrollo de software modernas han encontrado formas de escalar sus desarrollos al dividir el desarrollo de un sistema entre varios equipos. Cada equipo construye servicios bajamente acoplados sin pisar los callos de los demás equipos para después integrar estos servicios en un gran sistema cohesivo. El secreto a voces más reciente, microservicios, se enfoca en exactamente eso.

Separar tu sistema en varios pequeños servicios regularmente significa que estos servicios necesitan comunicarse entre ellos vía ciertas (en algunas ocasiones bien definidas, en algunas otras crecidas de manera accidental) interfaces.

Las interfaces entre las diferentes aplicaciones pueden venir en diferentes formas y sabores tecnológicos. Los más comunes son:

* REST y JSON via HTTP(S)
* RPC utilizando algo como gRPC
* SOAP y XML via HTTP(S)
* Construir una arquitectura orientada a eventos utilizando queues.

Para cada interface existen dos partes involucradas: El proveedor y el consumidor. El **proveedor** entrega datos a los consumidores. El **consumidor** procesa los datos obtenidos del proveedor. En el mundo REST un proveedor construye un API REST con todos los endpoints requeridos; un consumidor hace invocaciones al API REST para obtener datos o disparar cambios en el otro servicio. En el mundo asíncrono orientado a eventos, el proveedor (comúnmente llamado publicador) publica datos hacia una dirección (queue o tópico); un consumidor (comúnmente llamado suscriptor) se suscribe a estas direcciones para leer y procesar los datos.

// TODO Agregar imagen

Mientras se vaya dividiendo los servicios consumidores y proveedores entre los diferentes equipos nos encontraremos en la situación donde tenemos que especificar claramente la interface entre estos servicios, el famoso contrato. Tradicionalmente este problema ha sido enfrentado de la siguiente manera:

1. Escribir una especificación de la interface larga y detallada (el contrato)
2. Implementar el servicio que proveerá el contrato
3. Arrojar la especificación de la interface por la borda hacia el equipo consumidor.
4. Esperar a que ellos implementen el consumo de la interface
5. Ejecutar algunas pruebas de sistema a gran escala manualmente para verificar que todo funciona
6. Esperar a que ambos equipos se hayan apegado a la definición para siempre y no regarla.

Algunos equipos de desarrollo de software han reemplazado los pasos 5 y 6 con algo más automatizado: Pruebas automatizadas de contrato que aseguran que las implementaciones de ambas partes, consumidor y publicador, se apegan al contrato. Además funcionan como un buen conjunto de pruebas de regresión que aseguran que las desviaciones del contrato se notifiquen lo más temprano posible.

En una organización más ágil debemos de tomar un camino más eficiente y menos desaprovechado. Si construimos nuestras aplicaciones en la misma compañía, no debería ser tan difícil hablar directamente con los desarrolladores de los otros servicios en vez de arrojarles documentación sobre la barda. Después de todo ellos son compañeros de trabajo y no un vendedor al cual solo le puedes hablar a través de soporte a clientes o contratos legales a prueba de balas.

## Consumer-Driven Contract (CDC) tests

Este tipo de pruebas permiten a los consumidores implementar un contrato. Utilizando CDC, los consumidores de una interface escriben pruebas que verifican la interface para todos los datos que necesitan de esa interface. El equipo consumidor publica estas pruebas de tal modo que el equipo publicador puede obtener y ejecutar estas pruebas fácilmente. El equipo proveedor puede desarrollar su API ejecutando las pruebas CDC. Una vez que todas las pruebas son exitosas, saben que han implementado todo lo que el equipo consumidor necesita.

//TODO Colocar imagen que ilustre párrafo anterior.

Este enfoque permite al equipo proveedor implementar únicamente lo que es realmente necesario, como lo dice TDD, evitando sobreingeniería y manteniendo las cosas simples. El equipo que provee la interface debe obtener y correr estas pruebas CDC continuamente (en su proceso de integración continua - CI o pipeline de integración) para alertar cualquier cambio que haga incumplir con el contrato establecido de manera inmediata. Si ellos violan el contrato, sus pruebas CDC fallarán, evitando así que estos cambios se vayan hasta producción. Siempre y cuando las pruebas se mantengan en verde el equipo puede hacer tantos cambios como quiera sin tener que preocuparse de los otros equipos. Este enfoque, Consumer-Driven Contract nos dejaría con un proceso similar a este:

* El equipo consumidor escribe pruebas automatizadas con todas las expectativas de los consumidores
* Publican estas pruebas para el equipo proveedor
* El equipo proveedor ejecuta las pruebas CDC continuamente y las mantiene en verde.
* Ambos equipos hablan entre ellos una vez que las pruebas CDC dejan de funcionar.

En un esquema de microservicio, tener las pruebas CDC es un gran paso hacia establecer equipos autónomos. Las pruebas CDC son una forma automatizada de fomentar la comunicación entre equipos. Éstas aseguran que las interfaces entre equipos están funcionando todo el tiempo. Al haber una falla en las pruebas CDC es un buen indicador de que debes hablar con el equipo afectado, platicar sobre cualquier cambio por venir en el API y encontrar la mejor manera de realizarlo.

Una ingeniosa implementación de las pruebas CDC puede ser tan simple como disparar solicitudes a un API y verificar que las respuestas contienen todo lo que necesitas. Entonces empaquetas estas pruebas como un ejecutable (jar, sh) y lo subes a un lugar donde el otro equipo las pueda obtener. Por ejemplo un repositorio de artefactos.

En los últimos años el enfoque de CDC ha sido más y más popular y muchas herramientas se han creado en torno a CDC que permiten implementarlas e intercambiarlas más fácilmente.

Pact es la más prometedora en estos días. Cuenta con un enfoque sofisticado de escribir pruebas tanto para el consumidor como el proveedor, te entrega stubs para otros servicios y permite que intercambies las pruebas CDC con otros equipos. Pact ha sido llevado a muchas plataformas y puede ser usado con lenguajes de la JVM, Ruby, .Net, Javascript y muchos más.

Las pruebas de contrato orientadas al consumidor (CDC) pueden ser un factor real de cambio para establecer equipos autónomos que puedan moverse rápido y con confianza. Démosle una oportunidad. Un conjunto sólido de pruebas CDC es invaluable al permitirnos mover más rápido sin descomponer otros servicios y generar mucha frustración con otros equipos.