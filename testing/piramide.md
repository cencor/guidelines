# Pirámide de pruebas

Si realmente queremos ponernos serios con respecto a la automatización de pruebas para nuestro software existe un concepto clave que todos debemos saber: La **pirámide de pruebas**. Mike Cohn obtuvo y propuso este concepto en su libro "Succeeding with Agile". Es una grandiosa metáfora visual que invita a pensar en diferentes etapas de pruebas. También nos dice qué tantas pruebas hacer en cada etapa.

![Pirámide de pruebas](piramide.jpg)

La pirámide de pruebas original de Mike Cohn consiste en tres capas que debe de cumplir tu conjunto de pruebas (de abajo a arriba):

* Pruebas unitarias
* Pruebas de servicio
* Pruebas de interface de usuario

Desafortunadamente el concepto de pirámide de prueba se queda un poco corto si miramos a detalle. Algunos pueden argumentar que el nombre o algunos conceptos de la pirámide de pruebas no son ideales. Desde un punto de vista más moderno la pirámide de pruebas parece muy simple y por ello se puede malinterpretar.

De cualquier manera la sencillez de la pirámide de pruebas sirve como regla de oro cuando se trata de establecer nuestro propio conjunto de pruebas. Lo mejor que podemos hacer es recordar dos cosas sobre la pirámide de pruebas original:

* Generar pruebas de diferente granularidad
* Mientras más alto el nivel, debemos de tener menos pruebas.

Mantengamos la forma de una pirámide para obtener un conjunto de pruebas saludable, rápido y mantenible: Escribe muchas, muchas, pruebas unitarias pequeñas y rápidas. Escribe algunas pruebas más gruesas o de integración y muy pocas pruebas de alto nivel donde verificas tu aplicación de punta a punta.

Debemos tener cuidado de no terminar con un **cono de helado de pruebas** que será una pesadilla para mantener y toma mucho tiempo para ejecutarse. En el cono de helado de pruebas la pirámide se invierte: Teniendo muy pocas pruebas unitarias, en mayor medida pruebas de servicio y en un tamaño muy grande pruebas de punta a punta. Es muy importante no olvidar que mientras más arriba vamos, las pruebas son más difíciles de automatizar y generalmente lo que sería la bola del helado se convierten en pruebas de regresión manuales.

![Pirámide de pruebas](cono.jpg)