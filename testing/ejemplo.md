# Aplicación de ejemplo

## Herramientas y librerías que utilizaremos

* JUnit: El responsable de ejecutar las pruebas
* Mockito: Para mockear (simular) dependencias
* Wiremock: Para simular (stub) servicios externos
* RestTemplate: Para escribir pruebas punta a punta de APIs REST

## La aplicación de ejemplo

Se ha escrito un microservicio sencillo que incluye un conjunto de pruebas para las diferentes **capas** o niveles de la **pirámide de pruebas**. Esta aplicación muestra algunas características y rasgos de un microservicio típico, basado en **DDD** y una **arquitectura hexagonal**. Proveé una interface REST, se relaciona con una base de datos y consume un servicio web externo REST. Está implementada con Spring boot y debe ser entendible inclusive si nunca has trabajado con Spring Boot anteriormente.

### Funcionalidad

La funcionalidad de la aplicación es simple. Ofrece una interface REST con un endpoint:

```
//TODO colocar context path
```

### Vista de componentes generales


### Arquitectura (interna) hexagonal

La arquitectura interna está basada en una arquitectura hexagonal con:

* **Adapdatores** Por ejemplo controladores REST, clientes de servicios REST, suscriptores de mensajes (JMS, Kafta), publicadores de mensajes, entre otros. Básicamente todo aquello que nos comunica con el mundo exterior.
* **Servicios de dominio** Responsables de ejecutar tareas de integración sobre un aggregate
* **Modelo de dominio - Aggregates**
* **Repositorio** Responsable de persistir y recuperar un aggregate