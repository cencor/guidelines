# Implementando una prueba unitaria

Ahora que ya sabemos que probar y como estructuras nuestras pruebas unitarias finalmente podemos ver un ejemplo real.

Observemos una versión simplificada de la clase `GreetingController`:

```
@RestController
public class GreetingController {

    private static final String TEMPLATE = "Hello %s %s!";

    @Autowired
    private PersonRepository personRepository;

    @RequestMapping(path = "/greeting/lastname/{lastName}", method = RequestMethod.GET)
    public final Greeting greeting(@PathVariable String lastName) {
        Optional<Person> persons = personRepository.findByLastName(lastName);

        return persons.map(person -> new Greeting(person.getId(), 
            String.format(TEMPLATE, person.getName(), person.getLastName())))
            .orElseThrow(() -> new NoSuchElementException("Quien es esa persona con apellido " + lastName + "?"));
    }
}
```

Una prueba unitaria para el método `greeting(lastName)` puede parecerse a esto:

```
@SpringBootTest
public class GreetingControllerTest {

    @MockBean
    private PersonRepository personRepository;

    @Autowired
    private GreetingController greetingController;

    @Test
    public final void shouldReturnAGreetingWithFullName() {
        //Given
        String lastName = "Wazowski";
        Person mike = new Person(1L, "Mike", "Wazowski", new Date(System.currentTimeMillis()));
        Mockito.when(personRepository.findByLastName(lastName)).thenReturn(Optional.of(mike));
        
        //When
        Greeting greeting = greetingController.greeting(lastName);
        
        //Then
        Assertions.assertEquals(1L, greeting.getId());
        Assertions.assertEquals("Hello Mike Wazowski!", greeting.getContent());
    }

    @Test
    public final void personIsUnknown() {
        //Given
        String lastName = "Randal";
        Mockito.when(personRepository.findByLastName(lastName)).thenReturn(Optional.empty());

        //When (greetingController.greeting(lastName)) & Then (Arroja una excepcion del tripo NoSuchElementException)
        Assertions.assertThrows(NoSuchElementException.class, () -> greetingController.greeting(lastName));
    }
}
```

Estamos escribiendo las pruebas unitarias utilizando JUnit, el de-facto framework de pruebas estándar para Java. Usamos Mockito para reemplazar a la clase real `PersonRepository` con un mock para nuestra prueba. Este mock nos permite definir respuestas preparadas que este mock debe regresar para esta prueba. Mockear hace nuestra prueba más simple, predecible y nos permite asignar datos de prueba fácilmente.

Siguiendo la estructura "Configurar datos de prueba, ejecutar y evaluar", escribimos un par de pruebas unitarias, una positiva y una donde la persona buscada no fue encontrada. La primera, caso de prueba positivo, crea una nueva persona y le indica al repositorio mock que devuelva este objeto cuando se busque por el apellido Wazowski. La prueba entonces invoca el método que queremos probar. Finalmente verifica que la respuesta es igual a la esperada.

La segunda prueba funciona de manera similar pero prueba el escenario donde el método probado no devuelve ninguna persona para el apellido proporcionado. Entonces arroja una excepcion