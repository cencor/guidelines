# Pruebas de integración

Cualquier aplicación no trivial se integrará con algunas otras partes (bases de datos, filesystems, invocaciones de red hacia otras aplicaciones). Al escribir pruebas unitarias estas partes son las que comúnmente dejamos fuera para poder tener un mayor aislamiento y pruebas más rápidas. Aun así, tu aplicación interactúa con otras partes y éstas necesitan ser probadas. **¡Las pruebas de integración están aquí para ayudar!** Estas pruebas verifican la integración de tu aplicación con todas las partes que viven fuera de tu aplicación.

Para tus pruebas automatizadas esto significa que no sólo necesitas ejecutar tu aplicación sino también los componentes con los que te estás integrando. Si estás probando la integración con una base de datos necesitas tener disponible una base de datos cuando ejecutas tus pruebas. Para pruebas en las que necesites leer archivos desde un disco tu necesitas guardar un archivo en el disco y cargarlo en tu prueba de integración.

Se mencionó anteriormente que las pruebas unitarias son un término muy vago, esto es aún más cierto para las pruebas de integración. Para algunas personas las pruebas de integración significan verificar a lo largo de todo el stack de tu aplicación conectada con otras aplicaciones de tu sistema. Es mucho mejor tratar las pruebas de integración más estrechamente y verificar un punto de integración a la vez, **reemplazando los otros servicios y bases de datos con dobles de pruebas**. Junto con las pruebas de contrato y ejecutando las pruebas de contrato contra dobles podemos tener pruebas de integración que son más veloces, más independientes y usualmente más fáciles de razonar.

Las pruebas de integración angostas o estrechas viven en las fronteras de tu servicio. Conceptualmente siempre disparan una acción que nos lleva a integrarnos con alguna parte externa (filesystem, base de datos, algún servicio separado).

Una prueba de integración con base de datos puede parecerse a esto:

//TODO Agregar imagen

* Inicializar una base de datos.
* Conectar tu aplicación con la base de datos.
* Disparar una función dentro de tu código que escriba datos en la base de datos.
* Verificar que los datos hayan sido realmente escritos en la base de datos mediante la lectura de estos datos desde la base de datos.

Otro ejemplo, probar que tu servicio se integra con otro servicio a través de una API REST puede parecerse a esto:

//TODO Agregar imagen

* Inicializar tu aplicación.
* Iniciar la instancia del otro servicio (o un doble de pruebas con la misma interface).
* Disparar una función dentro de tu código que lea del otro servicio.
* Verificar que tu aplicación puede parsear correctamente la respuesta.

Tus pruebas de integración, como las unitarias, pueden ser mucho como caja blanca. Algunos frameworks te permiten inicializar tu aplicación y al mismo tiempo permitirte mockear algunas partes de tu aplicación para que tú puedas verificar que las interacciones correctas han sucedido.

Escribe pruebas de integración para todas las piezas de código donde serializas o deserializas datos. Esto ocurre mucho más frecuentemente de lo que puedes imaginar. Piensa en:

* Llamadas a tus servicios REST
* Leer desde y escribir hacia bases de datos
* Llamadas de otros servicios web (Por ejemplo SOAP)
* Leer desde y escribir hacia queues.
* Escribir hacia el filesystem

Escribir pruebas de integración alrededor de estos límites asegura que escribir hacia y leer desde estos colaboradores externos funciona correctamente.

Al escribir este tipo de pruebas de integración debes aspirar a ejecutar tus dependencias externas localmente: Puedes configurar una base de datos en memoria, verificar contra un filesystem ext4 local. Si estás integrando con un servicio externo puedes ejecutar una versión doble del servicio que simule el comportamiento del servicio real.

Evita integrarte con los servicios reales en tus pruebas automatizadas. Ejecutar miles de pruebas hacia algún servicio real puede 

* Provocar el enojo de varias personas porque estás inundando los logs (en el mejor de los casos)
* O peor aún provocar un DoS en sus servicios. 

Recuerda que integrarse con un servicio a través de la red es una característica de pruebas de integración amplias y hacen tus pruebas más lentas y usualmente difíciles de escribir y controlar. Adicionalmente no tienes control de los datos de prueba lo cual puede llegar a hacer inmantenibles tus pruebas resultando en dejar de utilizarlas.