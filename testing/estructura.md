# Estructura

Una buena estructura para todas tus pruebas (y no se limita únicamente a unitarias) es la siguiente:

* **Configura los datos de prueba o precondiciones.** 0..* - En una prueba no necesariamente tienes precondiciones
* **Invoca el método que será probado.** 1 Evita tener multiples acciones, de hacerlo no sabrás cuál estás probando. Limítalo a 1 sola acción a probar
* **Verifica los resultados esperados.** 1..* Al menos deberías de tener una validación.

Puedes recordar fácilmente esta estructura tomando inspiración de BDD. El famoso **Given, When, Then** donde **Given** representa la configuración o precondiciones, **When** el método o acción que se invoca y **Then** la parte de verificación.

Este patrón se puede aplicar para otras pruebas de más alto nivel. En cada caso de prueba se asegura que tus pruebas se mantienen fáciles de leer y entender. Por encima de eso las pruebas escritas con esta estructura en la mente tienden a ser más cortas y más expresivas.
