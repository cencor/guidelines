# Integración con otros servicios

Nuestro microservicio habla con el servicio de mensajería Slack, un API REST de envío de mensajes hacia distintos canales. Por supuesto que queremos asegurar que nuestro servicio envía las solicitudes y parsea las respuestas correctamente. Echemos un vistazo al método send de la clase SlackPoster responsable de enviar las peticiones al API de Slack.

```
@Override
public final boolean post(final String message) {
    SlackMessage slackMessage = new SlackMessage(message);
    LOGGER.info("Enviando mensaje a slack");
    try {
        restTemplate.postForEntity(url, slackMessage, String.class);
        LOGGER.info("Mensaje enviado");
        return true;
    } catch (RestClientResponseException e) {
        LOGGER.error("No se ha podido entregar el mensaje. Error: {}, respuesta: {}", 
        e.getRawStatusCode(), e.getResponseBodyAsString());
        return false;
    }
}
```

Queremos evitar hablar con el servicio real de Slack al estar ejecutando nuestras pruebas automatizadas. Los costos de por ejemplo enviar una notificación a un canal de alertas de Slack son tan altos como dejar de confiar en un esquema de alertamiento que es dañado por la metáfora de "Pedro y el Lobo". Esto es sólo una de las varias razones por la cual no utilizar el servicio real. La razón real es el **desacoplamiento**. Nuestras pruebas deben de **correr independientemente** de cualquier cosa que la apreciable gente de Slack esté haciendo. Inclusive cuando nuestros equipos no puedan alcanzar los servicios de Slack o los servicios de Slack se encuentren fuera de servicio (Aunque sea Slack esto puede suceder).

Podemos evitar pegarle a los servicios reales de Slack al correr uno propio, fake, mientras estamos ejecutando nuestras pruebas de integración. Esto puede sonar algo como una tarea muy grande. Afortunadamente existen herramientas como JMockit y Wiremock que hacen esta tarea muy fácil. Echa un vistazo:

```
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureWireMock(port = 0)
public class SlackPosterTest {

    /***/
    @Autowired
    private MessagePoster poster;

    /***/
    @Test
    public final void sendMessageSuccesfully() {
        String expectedJson = "{\"text\":\"Hola mundo!\"}";
        String context = "/services/T1KFZAZKR/BBRK1P4BX/VRYZzkzx9w1SgFMFAJhTopdZ";
        WireMock.stubFor(
              post(urlPathEqualTo(context))
              .withRequestBody(equalToJson(expectedJson))
              .willReturn(aResponse()
              .withHeader("content-type", MediaType.APPLICATION_JSON_VALUE)
              .withBody("ok")
              .withStatus(HttpStatus.OK.value())));

        boolean result = poster.post("Hola mundo!");

        WireMock.verify(postRequestedFor(urlPathEqualTo(context))
            .withRequestBody(equalToJson(expectedJson)));
        Assert.assertTrue(result);
}


    /***/
    @Test
    public final void sendMessageWithInvalidToken() {
        String expectedJson = "{\"text\":\"Hola error!\"}";
        String context = "/services/T1KFZAZKR/BBRK1P4BX/VRYZzkzx9w1SgFMFAJhTopdZ";

        WireMock.stubFor(
              post(urlPathEqualTo("/services/T1KFZAZKR/BBRK1P4BX/VRYZzkzx9w1SgFMFAJhTopdZ"))
              .withRequestBody(equalToJson(expectedJson))
              .willReturn(aResponse()
              .withHeader("content-type", MediaType.APPLICATION_JSON_VALUE)
              .withBody("Bad token")
              .withStatus(HttpStatus.NOT_FOUND.value())));

        boolean result = poster.post("Hola error!");

        WireMock.verify(postRequestedFor(urlPathEqualTo(context))
              .withRequestBody(equalToJson(expectedJson)));
        Assert.assertFalse(result);
}
```

Para usar Wiremock instanciamos un WireMockRule en un puerto aleatorio (Cuando el puerto es 0 se genera en un puerto aleatorio, muy util utilizar puertos aleatorios al correr pruebas en paralelo). Utilizando el DSL de WireMock podemos configurar el servidor de Wiremock, definir los endpoints que debe escuchar y colocar las respuestas preparadas que debe responder.

Enseguida invocamos el método que deseamos probar, poster.post(...), el que invoca el servicio externo o de un tercero y verificar si el resultado se pudo parsear correctamente. Este ejemplo es muy sencillo y la respuesta del servicio real es simplemente un texto:

```
"ok"
```

Por lo que únicamente validamos que el parseo de ese identificador sea correcto. En casos más completos puedes verificar que el parseo de la respuesta a un DTO por ejemplo se de apropiadamente.

Es importante entender como nuestra prueba sabe que debe de llamar al servicio fake de Wiremock en vez del API del servicio real del notificador. El secreto está en nuestro application.properties que se ubica en src/test/resources. Este es el archivo properties que Spring carga al ejecutar las pruebas. En este archivo sobreescribimos configuración como API Keys y URLs con valores que son ajustados para nuestros propósitos de pruebas. Por ejemplo, invocar el servidor fake de Wiremock en vez del servicio real:

```
#application.properties ubicado en /src/test/resources

#El application.properties ubicado en /src/main/resouces muy probablemente asignará 
#las propiedades que cambien por cada ambiente de pruebas a variables de ambiente
#para externalizar su valor y así evitar construir el binario por ambiente.

slack.webhook.alertschannel.url=http://localhost:${wiremock.server.port}/services/T1KFZAZKR/BBRK1P4BX/VRYZzkzx9w1SgFMFAJhTopdZ
```

Echa un vistazo al puerto definido aquí pues debe se sobreescribe por el configurado aleatoriamente en la prueba al crear la instancia de WireMockRule. Reemplazar la URL real del API del notificador con una fake para nuestras pruebas es posible al inyectar la URL en nuestro RestEmailer en un atributo que Spring nos ayuda a instanciar a través del constructor:

```
/**
* @param restTemplate La plantilla REST para realizar la peticion.
* @param url La direccion a donde se realizara la peticion.
*/
public SlackPoster(@Autowired final RestTemplate restTemplate, 
        @Value("${slack.webhook.alertschannel.url}") final String url) {
    this.restTemplate = restTemplate;
    this.url = url;
}
```

Aunque pudieras hacerlo directamente en el atributo:

```
@Value("${slack.webhook.alertschannel.url}")
private String url;
```

Siempre es preferible hacerlo a través del **constructor**, siguiendo al pie de la letra la **inyección de dependencias**, pues de esta forma no nos atamos al esquema de inyección de dependencias del framework, en este caso Spring, si no que permitimos la inyección de dependencias con o sin framework a partir del constructor.

De esta forma le decimos a nuestra clase SlackPoster que asigne el valor de url con el valor de la propiedad slack.webhook.alertschannel.url que definimos en nuestro application.properties

Como puedes ver, escribir pruebas de integración acotadas con otros servicios es bastante fácil con herramientas como Wiremock. Desafortunadamente existe una debilidad: ¿Cómo podemos asegurar que el servicio fake se comporta como el servicio real? Con la implementación actual, el otro servicio pudiera cambiar su API y nuestras pruebas pudieran seguir funcionando sin darnos cuenta del cambio. Usar pruebas end-to-end y correr estas pruebas contra las instancias de los servicios reales nos daría mayor confianza sobre el resultado de nuestras pruebas pero nos haría completamente dependientes tanto de la disponibilidad del otro servicio como de los datos de prueba. En nuestro ejemplo no pudiéramos asegurar que la respuesta (correlationId) sea 1 pues el servicio real incrementa ese número por cada petición.

Afortunadamente existen otras prácticas que nos pueden ayudar a resolver este dilema. Una de ellas es adoptar el lineamiento Contract-first con todo lo que involucra, por ejemplo ser backwards-compatible. ¿Qué nos puede ayudar a verificar que nuestra buena voluntad no pase por alto esto? ¡Tener pruebas de contrato contra el servicio fake y el real! Esto asegura que el servidor fake que utilizamos en nuestras pruebas de integración es un doble de pruebas legítimo. De este modo podemos sacar total provecho a nuestras pruebas realizadas con JUnit que nos dirán en un par de minutos sí hemos roto algo o no, maravilloso ¿no? Echemos un vistazo cómo en la siguiente sección.