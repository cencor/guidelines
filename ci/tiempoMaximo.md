# Establece un tiempo máximo para corregir en vez de revertir.

Establece una regla de equipo: Cuando la construcción falla al entregar un cambio, intenta corregirlo en 10 minutos. Si después de diez minutos no has terminado con la solución, revierte hacia la revisión anterior.