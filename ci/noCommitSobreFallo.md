# No hagas commit/push sobre una construcción fallida.

El pecado capital de la integración continua es agregar cambios (commit/push) sobre una construcción fallida. Si la construcción falla, los desarrolladores responsables identifican la causa del fallo y lo corrigen tan pronto como sea posible. Adoptando esta estrategia siempre estaremos en la mejor posición para saber qué causó la falla y corregirlo inmediatamente.

Cuando esta regla no se cumple, inevitablemente tomará más tiempo para la que la construcción sea exitosa. La gente se acostumbra a ver la construcción fallando y muy rápidamente te encontrarás en una situación donde la construcción está fallando todo el tiempo. Esto continúa así hasta que alguien del equipo decide que ya es suficiente, un esfuerzo heroico para volver a tener una construcción exitosa. Después el mismo proceso se repite de nuevo.
