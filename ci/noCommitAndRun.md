# No commit & run (Nunca te vayas a casa con una construcción fallida)

Son 5:30 pm en viernes, tus compañeros se comienzan a retirar y tú acabas de entregar tus cambios. La construcción ha fallado. Tienes tres opciones. Te resignas a que te vas a ir tarde e intentas corregirlo, puedes revertir tus cambios y volver a intentar entregar tus cambios la próxima semana o simplemente te vas dejando la construcción fallando.

Si dejas la construcción fallando, cuando regreses el lunes no tendrás memoria fresca sobre los cambios que realizaste y te tomará considerablemente más entender el problema y corregirlo. Si no eres la primer persona corrigiendo el problema el lunes siguiente, tu nombre será mencionado muchas veces dentro del equipo pues la construcción está fallando y no pueden continuar trabajando. Peor aún si te enfermas el fin de semana y no te puedes presentar a trabajar al siguiente día hábil, no podrás esperar menos que muchas llamadas preguntando cómo corregir o que alguien deshaga tus cambios abruptamente. De cualquier manera tu nombre será muy sonado.

El efecto de una construcción fallida generalmente, y especialmente si es al final del día, se maximiza cuando se trabaja con un equipo distribuido con grupos en diferentes usos horarios o ubicaciones físicas.

Sólo para estar absolutamente claros, no se está recomendando quedarse tarde a corregir la construcción después de la salida. Más bien, se recomienda que entregues tus cambios regularmente y lo suficientemente temprano para que tengas tiempo de tratar con problemas si es que ocurren. Alternativamente, guarda tu cambio para el siguiente día hábil.

Algunos sistemas de control de versiones, incluyendo todos los distribuidos como Git, hacen esto más fácil al permitir que puedas acumular varios cambios en tu repositorio local sin entregarlos a los demás.
