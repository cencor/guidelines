# Prerequisitos

## Ambiente de desarrollo

Es importante para la productividad y cordura de los desarrolladores que su ambiente de desarrollo sea tratado cuidadosamente. Los desarrolladores siempre deben trabajar desde un punto inicial bueno cuando comienzan a agregar código fresco. Deben de ser capaces de correr la construcción, ejecutar pruebas automatizadas y desplegar la aplicación en un ambiente bajo su control. En general, esto debe ser en su propia máquina local. Únicamente en circunstancias especiales se debe contar con estos ambientes compartidos para desarrollo. Correr la aplicación en un ambiente local debe usar el mismo proceso automatizado que es utilizado por el servidor de integración continua, ambientes de pruebas e inclusive de producción.

El primer prerrequisito para completar esto es una administración de la configuración cuidadosamente, no solo el código sino también datos de prueba, scripts de BD, scripts de construcción y scripts de despliegue. Todo esto debe estar guardado en el control de versiones, y la versión más reciente y buena de cada uno de estos elementos debe ser el punto inicial cuando se comienzan los desarrollos. En este contexto, bueno significa que la revisión en la que estás trabajando ha pasado todas las pruebas

El segundo paso es configurar las dependencias, librerías y componentes de terceros. Es vital que tu cuentes con las versiones correctas de librerías o componentes.Existen herramientas libres para administrar las dependencias de terceros como Maven y Gradle.

El último paso es asegurar que las pruebas automatizadas, incluyendo pruebas de humo, puedan ser ejecutadas sobre las máquinas de los desarrolladores. En grandes sistemas esto puede involucrar configurar sistemas middleware y correr bases de datos en memoria. Esto puede involucrar un cierto grado de esfuerzo pero el habilitar a los desarrolladores correr una prueba de humo contra el sistema en su propia computadora antes de hacer un check-in puede hacer una diferencia muy grande en la calidad de tu aplicación. De hecho, una señal de buen diseño de arquitectura es permitir que la aplicación pueda ser inicializada sin mucho esfuerzo en una computadora de desarrollo.

## Haz commit/push con regularidad

La práctica más importante para que integración continua funcione apropiadamente es hacer check-in frecuentemente hacia el trunk o la línea principal. Debes de estar entregando tus cambios al menos un par de veces al día.

Hacer check-in con regularidad trae muchos beneficios. Hace tus cambios más pequeños y por lo tanto menos probable de hacer fallar la construcción. Significa que tienes una versión reciente y buena del software cuando tienes que deshacer o revertir un cambio. Ayuda a que seas más disciplinado sobre refactorizaciones apegandote a cambios pequeños que mantienen el comportamiento. Ayuda a asegurar que los cambios que involucran muchos archivos sean menos probable de causar conflicto con el trabajo de otras personas. Permite a los desarrolladores ser más exploradores, intentando nuevas ideas, descartandolas. Forza a que tomes descansos regularmente y estires tus músculos para evitar el síndrome del túnel carpiano. También significa que is algo catastrófico sucede (como borrar algo por error) no has perdido mucho trabajo.

Se menciona hacer check-in sobre trunk a propósito. Muchos proyectos utilizan branches en el sistema de control de versiones para organizar equipos grandes. Pero es imposible hacer integración continua correctamente al estar usando branches porque, por definición, si estás trabajando sobre un branch, tu código no está siendo integrado con el de otros desarrolladores. Los equipos que utilizan branches de larga duración se enfrentan exactamente a los mismos problemas de integración que se describen en la introducción.

## Crea un conjunto comprensivo de pruebas automatizadas

Si no cuentas con un conjunto comprensivo, una construcción exitosa únicamente significa que la aplicación puede ser compilada y ensamblada. Aunque para algunos equipos esto es un gran paso, es esencial tener un cierto nivel de pruebas automatizadas que generen la confianza de que la aplicación está funcionando. Existen muchos tipos de pruebas automatizadas y son discutidas en una sección dedicada. De cualquier manera, existen tres tipos de pruebas en las cuales estamos interesados en correr desde nuestra construcción de integración continua: pruebas unitarias, pruebas de componente y pruebas de aceptación.

Las pruebas unitarias sirven para probar el funcionamiento de pequeños fragmentos de tu aplicación de manera aislada (por ejemplo, un método o función, o las interacciones entre un grupo pequeño de funciones). Usualmente se pueden correr sin inicializar toda la aplicación. No llegan hasta la base de datos (si tu aplicación usa una), el sistema de archivos o la red.

Las pruebas de componente prueban el comportamiento de distintos componentes de tu aplicación. Como las pruebas unitarias, no siempre requieren inicializar toda la aplicación. Aunque pueden llegar a la base de datos, el sistema de archivos u otros sistemas (que pueden ser stubs o mocks).

Las pruebas de aceptación prueban que la aplicación cumple con los criterios de aceptación definidos por el negocio, incluyendo tanto funcionalidad como atributos de calidad como capacidad, disponibilidad, seguridad y demás. Las pruebas de aceptación están mejor escritas de tal forma que puedan ser ejecutadas en la aplicación en un ambiente similar a producción. Las pruebas de aceptación pueden tomar mucho tiempo en ejecutarse, algunas de ellas pueden tomar más de un día (aunque sean automatizadas)

Estos tres tipos de pruebas en conjunto deben de proveer un extremadamente alto nivel de certidumbre sobre algún cambio que haya provocado que la funcionalidad existente deje de funcionar.

## Mantén el proceso de construcción y pruebas corto.

Si toma mucho tiempo en construir el código y ejecutar las pruebas, podrás caer en los siguiente problemas:

La gente dejará de hacer la construcción y ejecución de todas las pruebas antes de hacer un check-in y comenzarás a tener más construcciones fallidas.

El proceso de integración continua tomará tanto que habrán múltiples commits entre cada construcción que te costará trabajo saber cuál de todos ellos ocasionó que dejara de funcionar la construcción.

Las personas harán menos check-in porque se tendrán que esperar mucho tiempo para que la construcción y las pruebas sean ejecutadas.

Idealmente el proceso de compilación y pruebas que ejecutes antes de un check-in y después en tu servidor de integración continua debe tomar unos pocos minutos. Pensamos que 5 minutos está cerca del límite máximo y alrededor de 90 segundos o menos es lo ideal.

En algún punto tendrás que dividir tu proceso de pruebas en varias etapas. Puedes comenzar creando dos etapas. La primera debe compilar tu software, correr un conjunto de pruebas unitarias y crear un binario para desplegar. Esta etapa es la llamada etapa de commit.

La segunda etapa debe tomar los binarios de la primera etapa y correr las pruebas de aceptación, así como pruebas de integración y de rendimiento si las tienes.

La etapa de commit debe de ejecutarse antes de hacer check-in y deben ser ejecutadas en el servidor de CI por cada check-in.