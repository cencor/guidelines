# Prácticas esenciales

Hasta ahora, mucho de lo que se ha descrito ha sido relacionado con la automatización de la construcción y el despliegue. Aunque esa automatización existe dentro de un ambiente de procesos humanos, La integración continua es una práctica, no una herramienta y depende de disciplina para hacerla efectiva. Mantener un sistema de integración continua operando requiere de un cierto nivel de disciplina del equipo de desarrollo como una pieza.

El objetivo de nuestro sistema de integración continua es asegurar que nuestro software se encuentra funcionando, en esencia, todo el tiempo. Este conjunto de prácticas son indispensables para que la integración continua funcione correctamente.
