# Listo para revertir tus cambios a la revisión anterior.

Todos cometemos errores, así que espera que cualquiera rompa la construcción de vez en cuando. En proyectos grandes esto se puede presentar diariamente, aunque los commits preprobados localmente alivian mucho de este dolor. En estas circunstancias las correcciones normalmente son cosas sencillas que resultan en un cambio de una línea.

De cualquier manera en ocasiones nos equivocamos en más de una línea y no podemos encontrar el problema o simplemente nos acordamos de algo importante que va con la misma naturaleza del cambio.

Cualquiera que sea la causa de la falla, es importante que volvamos a funcionar rápidamente. Si no podemos resolver el problema rápidamente, por la razón que sea, debemos de revertir hacia la revisión anterior de nuestro control de versiones y remediar el problema localmente. Al final tu sabes que la revisión anterior funciona porque tu **no haces check in sobre una construcción fallida.**
