# Integración continua

Una característica extremadamente extraña, pero común, de muchos proyectos de software es que por largos periodos de tiempo durante el proceso de desarrollo la aplicación no se encuentra en un estado funcional. De hecho, mucho del software desarrollado por grandes equipos gastan una proporción de tiempo de desarrollo muy significante en un estado inusable. La razón de esto es fácil de entender: Nadie está interesado en intentar correr la aplicación completa hasta que esté terminada. Los desarrolladores entregan cambios e inclusive algunos ejecutan las pruebas unitarias automatizadas, pero nadie está intentando levantar la aplicación y usarla en un ambiente similar a producción.

Esto es doblemente verdadero en proyectos que usan banches de larga duración o difieren las pruebas de aceptación hasta el final. Muchos de estos proyectos planean grandes y lentas fases de integración al final del desarrollo para dar tiempo al equipo de desarrollo a integrar branches y a hacer funcionar la aplicación con el fin de que las pruebas de aceptación puedan ser realizadas. Peor aún, algunos proyectos encuentran que cuando llegan a esta etapa, su software no cumple su propósito. Estos periodos de integración pueden tomar un periodo de tiempo extremadamente largo, y lo peor de todo, nadie tiene una manera de predecir que tanto.

Por otra parte, existen proyectos que les toma unos pocos minutos estar en un estado donde su aplicación no está funcionando con los últimos cambios. La diferencia es el uso de integración continua. La integración continua requiere que cada vez que alguien realiza un cambio, la aplicación completa es construida y un conjunto comprensivo de pruebas automatizadas se ejecuta contra la aplicación. Fundamentalmente si la construcción o las pruebas fallan, el equipo de desarrollo detiene lo que sea que estén haciendo y corrigen el problema inmediatamente. El objetivo de la integración continua es que el software esté funcionando todo el tiempo.

Integración continua representa un cambio de paradigma. Sin integración continua, tu software no funciona hasta que alguien indique lo contrario, usualmente durante una etapa de pruebas o integración. Con integración continua tu software demuestra que funciona (asumiendo un conjunto suficientemente comprensivo de pruebas automatizadas) con cada cambio y sabes el momento exacto que se descompone y lo arreglas inmediatamente.

## Herramienta de CI para CENCOR

![Gitlab CI](gitlab-ci.png)

La herramienta para integración continua para CENCOR es Gitlab CI. Esta herramienta tiene una integración nativa con Gitlab además de poder respaldar junto con el código todo el historial de trabajos ejecutados por la integración continua a lo largo de la vida de cada proyecto de CENCOR.