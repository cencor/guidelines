# Prácticas sugeridas

## Falla la construcción por advertencias y violaciones de estilo de código o análisis estático.

Las advertencias del compilador o de una herramienta de análisis estático de código están usualmente advirtiendote por alguna buena razón. Para algunos proyectos fallar la construcción por cada advertencia puede ser excesivamente severo. Una estrategia para introducir esta práctica gradualmente es la matraca.

Esto significa comparar el número de cosas como advertencias, TODOs, reportes de sonar y findbugs con el número del anterior check in. Utilizando esta medida se puede hacer cumplir una política donde cada commit debe reducir el número de advertencias, TODOs, reportes de sonar y findbugs en al menos uno.

## Falla la construcción por pruebas lentas.

Como se ha mencionado antes, la integración continua funciona mejor con cambios pequeños y frecuentes. Si las pruebas de commit toman mucho tiempo para correr, puede tener un efecto muy negativo en la productividad del equipo al estar esperando a que el proceso de construcción y pruebas termine. Consecuentemente se inhibirá el hacer frecuentes check ins y el equipo comenzará a almacenar sus cambios en uno más grande, con mayor probabilidad de tener conflictos de integración y de agregar errores.

Si las pruebas corren rápidamente, los desarrolladores agregarán cambios con mayor frecuencia. Si los desarrolladores agregan cambios con más frecuencia, existe menos probabilidad de tener problemas de integración y si sale algún problema es muy probable que sea pequeño y rápido de resolver, por lo que los desarrolladores son más productivos

## Falla la construcción por violaciones de arquitectura.

En ocasiones hay aspectos de la arquitectura de un sistema que son muy fáciles de olvidar para los desarrolladores. Una técnica que se puede utilizar es colocar pruebas de commit que validen que no se esté violando la arquitectura.

Esta técnica es realmente táctica y difícil de describir más que con un ejemplo. Si tu componente no debe invocar URLs locales para cumplir un atributo de seguridad (arquitectura), evitando posibles ataques. Puedes generar un validador de URLs que falle si encuentra algún caso.