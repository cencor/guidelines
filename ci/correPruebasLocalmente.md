# Siempre corre todas las pruebas de commit localmente antes de entregar tus cambios.

Ejecutar las pruebas de commit localmente es una comprobación de validez antes de entregar los cambios. También es una forma de asegurar que lo que creemos que funciona realmente funciona.

Cuando los desarrolladores están listos para entregar sus cambios, deben refrescar su copia local del proyecto al actualizar del sistema de control de versiones. Ellos deben entonces ejecutar una construcción local y correr las pruebas de commit. Únicamente cuando esto es exitoso el desarrollador estará listo para entregar sus cambios al sistema de control de versiones.

Si no has realizado este procedimiento anteriormente, te podrás estar preguntando por qué correr las pruebas de commit localmente antes de entregar nuestros cambios si lo primero que sucederá al entregar nuestro código es que se compile y las pruebas de commit se ejecuten. Existen dos razones para efectuar este procedimiento:

Los otros desarrolladores pudieron haber agregado cambios antes de tu última actualización y la combinación de tus nuevos cambios con el de ellos puede causar que alguna o algunas pruebas fallen. Si tu actualizas y ejecutas las pruebas de commit localmente, identificarás el problema sin romper la construcción de todos.

Un problema muy común al agregar cambios es olvidar agregar algún archivo o artefacto al repositorio. Si tu sigues este procedimiento, tu construcción local es exitosa y luego el sistema de integración continua falla, sabrás que pudo fallar ya sea porque alguien metió un cambio justo antes que tú o porque olvidaste agregar una nueva clase o archivo de configuración al control de versiones.

Seguir esta práctica asegura que la construcción permanezca en verde (exitosa).
