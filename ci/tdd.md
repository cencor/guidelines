# Test-Driven Development

Tener un conjunto comprensivo de pruebas es esencial para integración continua. Vale mucho la pena resaltar que una rápida retroalimentación, que es la base de la integración continua, sólo es posible con una excelente cobertura de pruebas unitarias. En nuestra experiencia, la única manera de obtener una excelente cobertura de pruebas unitarias es a través de test-driven development.

Para aquellos que no están familiarizados con TDD, la idea es que cuando se desarrolle un nuevo fragmento de código o corrección de bug, los desarrolladores primero crean una prueba que es una especificación ejecutable del comportamiento esperado del código a escribir. No solamente estas pruebas conducen el diseño de la aplicación, también sirven como pruebas de regresión y documentación del código y del comportamiento esperado de la aplicación.

Recomendamos dos libros para estudiar con más detalle sobre este tema: **“Test-driven development by example”** de Kent Beck y **“Growing object oriented software guided by tests”** de Steve Freeman y Nat Pryce