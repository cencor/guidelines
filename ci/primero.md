# Antes de iniciar

## Control de versiones (Git y Gitlab)

Todo, absolutamente todo, lo relacionado a tu proyecto debe estar guardado en un repositorio de control de versiones: código, pruebas, scripts de base de datos, scripts de construcción y despliegue y todo lo demás para crear, instalar, ejecutar y probar tu aplicación. Esto puede sonar obvio pero sorprendentemente aún existen proyectos que no usan alguna forma de control de versiones.

## Construcción automatizada. (Gradle)

Se debe tener la capacidad de iniciar la construcción del proyecto desde la línea de comandos. Cualquiera que sea el mecanismo, debe ser posible tanto para una persona como una computadora correr la construcción, pruebas y proceso de despliegue de manera automatizada a través de la línea de comandos.

## Un sistema de integración continua básico. (Gitlab CI)

Realmente no necesitas un software de integración continua para poder hacer integración continua. La integración continua es una práctica no una herramienta.

Una vez que tienes la herramienta de integración continua, debe ser posible configurarla en unos pocos minutos con decirle donde está el código en el control de versiones, que script correr para compilar, ejecutar el conjunto de pruebas automatizadas y como notificar al equipo si es que el último conjunto de cambios ha roto el software.