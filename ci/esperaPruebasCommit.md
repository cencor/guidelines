# Espera a que las pruebas de commit pasen antes de continuar.

El sistema de integración continua es un recurso compartido por el equipo. Cuando un equipo usa la integración continua efectivamente, siguiendo la recomendación de agregar cambios frecuentemente, cualquier falla de la construcción es un pequeño tropiezo para el equipo y el proyecto.

De cualquier manera, las fallas en las construcciones son normales y parte del proceso. El objetivo es encontrar errores y eliminarlos tan pronto como sea posible.

Al momento de hacer la entrega, los desarrolladores que lo hicieron son responsables de monitorear el progreso de la construcción. Hasta que se haya compilado y las pruebas de commit hayan pasado los desarrolladores no pueden comenzar con otra tarea. No deben ir a comer o a alguna junta, deben de poner suficiente atención a la salida de la construcción.

Si la entrega es exitosa, los desarrolladores son, y únicamente entonces, libres de hacer otra tarea. Si falla, están disponibles para comenzar a determinar la naturaleza del problema y corregirlo, con un nuevo cambio o revirtiendo hacia la última versión estable, respaldando sus cambios hasta que sepan cómo corregirlos.

