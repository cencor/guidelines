# Modulos

Tener una estructura de proyectos estándar permite a cada uno de los ingenieros sentirse como en casa al participar en algún otro proyecto de la compañía.

La estructura para un proyecto esperado por CENCOR se describe a continuación. Es muy importante apegarse a esta estructura, tanto como sea posible.

## Modulo base

En Java se llaman paquetes y este paquete base comienza con el grupo CENCOR. Enseguida la empresa del grupo a la que pertenece la aplicación/servicio. Por último el nombre del servicio/contexto en el que se estará trabajando. Por ejemplo:

```
com.cencor.
           biva
           enlace
           mei(f|p)
                   dddContext|serviceName
```

### Ejemplos

#### Derechos de BIVA

`com.cencor.biva.derechos`

#### Fiduciario de MEI Presval

`com.cencor.meip.fiduciario`

#### Facturación de Enlace

`com.cencor.enlace.facturacion`

## Modulos internos

Los modulos o paquetes internos del servicio persiguen una arquitectura hexagonal. Donde se privilegia y protege al modelo de dominio. Envolviendolo con adaptadores, puertos, convertidores y demás herramientas que permitan su interacción con el mundo exterior sin revelar sus propiedades internas.

Dicho lo anterior la estructura de módulos (paquetes) queda de la siguiente forma:

```
domain // Paquete que engloba y representa el modelo de dominio, clasificado por tipo
   model // (Aggregates(sin sufijo) - Entidades de negocio, VO, Enum)
   service // Servicios de dominio que persisten, recuperan y ejecutan operaciones directamente a través de la Entidad
   event // Eventos de dominio que se generan en este dominio
   exception // Excepciones de dominio personalizadas que pueden ser lanzadas para cumplir ciertas restricciones del dominio

repository // Repositorios para poder persistir y recuperar el modelo de dominio

adapter // Adaptadores de clientes de servicios RPC (REST|SOAP)

controller // Adaptadores para publicación de servicios REST para que algún cliente los pueda consumir (REST|SOAP|LambdaController)

listener // Adaptador para mensajes de entrada (JMS|Kafka|WebSocket)

publisher // Adaptador para mensajes de salida (JMS|Kafka|WebSocket)

converter // Convertidores o mediadores para extraer la información de un objeto de dominio hacia un DTO|Recurso

dto // En este paquete se mantienen todos los DTOs con los que pudieramos estar trabajando, clasificándolos por tipo
   resource // Recursos REST que se estarían publicando en los servicios REST
   command // Comandos que se pueden recibir o enviar que representan una acción a ejecutar sobre un modelo de dominio
   event // Eventos que se puedan recibir de algún otro dominio en los que estamos interesados y así poder reaccionar.

application // Paquete que concentra cualquier otro elemento necesario para poder llevar a cabo las tareas del dominio que no sean propias del dominio.
   service
   dto
   util
   config
   exception
```

## Nombres de clases

Todas las clases que se estarían generando llevarían el sufijo del modulo al que pertenecen. De esta forma sería fácil identificar que tipo de clase es aquella con la que estemos trabajando.

La **excepción** del sufijo es para los Aggregates quienes **no** llevarían un sufijo. Esto pues para poder representar de forma más clara los objetos de dominio con los que se trabaja. Esta regla no aplica para los objetos de valor ni enumeraciones que viven en el mismo paquete. Estos últimos es importante diferenciarlos para poder dejar en claro que objetos son mutables (Aggregates o Entidades) y cuales son inmutables (Objetos de valor y enumeraciones)
