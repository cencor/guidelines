# Librerías de terceros/no registradas.

Es posible que existan librerías de terceros que no se encuentren en el repositorio maven central. Para esos casos es posible publicar al repositorio de CENCOR esas librerías. Por ejemplo las librerías necesarias para establecer una conexión con el protocolo financiero Indeval (PFI) o alguna otra librería que no se encuentre disponible en algún repositorio público.

## Publicación de librerías de terceros/no registradas

Para poder publicar dependencias de terceros en el repositorio de CENCOR (Gitlab Package registry), requerimos ayuda de la herramienta de construcción Maven:

1. Agregar en el archivo `M2_HOME/conf/settings.xml` la configuración de autenticación para el repositorio de CENCOR mediante el tag `server`:
```
...
<servers>
  <server>
    <id>gitlab-maven-cencor</id>
    <configuration>
      <httpHeaders>
        <property>
          <name>Deploy-Token</name>
          <value>REEMPLAZAR_POR_EL_TOKEN_DE_PUBLICACION_DE_ARTEFACTOS</value>
        </property>
      </httpHeaders>
    </configuration>
  </server>
</servers>
...
```

2. Publicar el artefacto mediante el siguiente comando:

```
mvn deploy:deploy-file -DgroupId={GROUP_ID} -DartifactId={ARTIFACT_NAME} \
-Dversion={ARTIFACT_VERSION} -Dpackaging=jar -Dfile="{RUTA_DEL_ARCHIVO_JAR}" \
-Durl={URL_REPOSITORIO_CENCOR} \
-DrepositoryId={ID_DEL_REPOSITORIO_CENCOR_CONFIGURADO_EN_SETTINGS_XML} \
-DpomFile="{RUTA_DEL_ARCHIVO_POM}" \
-Dclassifier="{PROFILE}"
```

Donde:

* **GROUP_ID** Es el nombre del grupo donde se almacenará el artefacto
* **ARTIFACT_NAME** Es el nombre del artefacto
* **ARTIFACT_VERSION** La versión del artefacto
* **RUTA_DEL_ARCHIVO_JAR** La ruta local donde se encuentra el jar del artefacto a publicar
* **URL_REPOSITORIO_CENCOR** La dirección del repositorio de CENCOR: `https://gitlab.com/api/v4/projects/37740056/packages/maven`. Aquí `37740056` es el id del repositorio privado de CENCOR donde se almacenarían este tipo de librerías internas, ubicado en: https://gitlab.com/cencor/internal-libraries
* **ID_DEL_REPOSITORIO_CENCOR_CONFIGURADO_EN_SETTINGS_XML** El id del `server` configurado en `settings.xml` en el punto 1. En ese caso es `gitlab-maven-cencor`
* **RUTA_DEL_ARCHIVO_POM** Opcional. La ruta local donde se encuentra el pom del artefacto a publicar, especialmente útil si es que la librería a publicar cuenta con dependencias transitivas. De no proporcionar alguno, se generará automáticamente uno **sin** dependencias transitivas
* **PROFILE** Opcional. Se incluye si la librería tiene algún perfil.

Por ejemplo:
```
mvn deploy:deploy-file -DgroupId=com.indeval.protocolofinanciero -DartifactId=protocolofinancieroAPI-C24 \
-Dversion=3.14 -Dpackaging=jar -Dfile="/tmp/protocolofinancieroAPI-C24-3.14.jar" \
-Durl=https://gitlab.com/api/v4/projects/37740056/packages/maven \
-DrepositoryId=gitlab-maven-cencor \
-DpomFile="/tmp/protocolofinancieroAPI-C24-3.14.pom" \
-Dclassifier=qa
```

Para utilizar este comando es necesario ejecutarlo con un java igual o superior a java 8.
Con está funcionalidad podemos centralizar todas las librerías necesarias para nuestros proyectos en un solo lugar. Un efecto secundario positivo es que no mezclamos nuestro código con los binarios de las librerías en nuestro sistema de control de versiones Git. Así nuestro proyecto se hace más ligero.

