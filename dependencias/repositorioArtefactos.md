# Repositorio de artefactos.

Aunado a la herramienta de construcción existe un repositorio de artefactos para CENCOR. Su principal función es mantener las dependencias creadas en CENCOR, en un solo repositorio que es accesible para todos los proyectos. Esto se **limita a mantener úinicamente aquellas dependencias que sean consideradas como librerías (bibliotecas) reutilizables**. Por ejemplo librería con implementación del patrón transactional outbox, reutilizable entre varios componentes, o alguna otra librería que contenga cierto código reutilizable entre componentes como manejo de errores.

El repositorio oficial de artefactos de [CENCOR](https://gitlab.com/groups/cencor/-/packages) es [Gitlab Package registry](https://docs.gitlab.com/ee/user/packages/maven_repository/). Responsable de respaldar todas las librerías reutilizables generadas por nuestros proyectos.

Además de este repositorio utilizado para almacenar las dependencias creadas en CENCOR, estaremos utilizando directamente el repositorio de maven central de donde se podrán obtener dependencias públicas. Por ejemplo Spring, Hibernate, Log4j, etc.

## Configuración de variable de ambiente

Es necesario que configures la siguiente variable de ambiente en tu equipo local: `CENCOR_PACKAGE_REPO_READ_TOKEN`. Por ejemplo:

```
export CENCOR_PACKAGE_REPO_READ_TOKEN=reemplazaPorElTokenDeLecura
```

### Linux

En Linux agrega la variable en alguno de los siguientes archivos:

* `~/.bashrc` Si lo quieres configurar solo para tu usuario.
* `/etc/bash.bashrc` Si lo quieres configurar de forma global en tu Linux

### MacOS

En Mac agrega la variable en el siguiente archivo:

* `~/.zshenv`

## Configuracion del repositorio de artefactos

Para poder obtener las dependencias tanto públicas como internas se requiere dar de alta los siguientes repositorios en `Gradle`:

```
repositories {
    mavenCentral()
    maven {
        url "https://gitlab.com/api/v4/groups/cencor/-/packages/maven"
        name "GitLab"
        credentials(HttpHeaderCredentials) {
            name = 'Deploy-Token'
            value = "$System.env.CENCOR_PACKAGE_REPO_READ_TOKEN"
        }
        authentication {
            header(HttpHeaderAuthentication)
        }
    }
}
```

Afortunadamente Gradle permite la reutilización de código y este fragmento de código lo encuentras en el repositorio [Build config](https://gitlab.com/cencor/governance/build-config/-/blob/main/repository/build-config-read-repo.gradle) de tal forma que puedes reutilizar este fragmento de código de la siguiente forma, tal como se muestra en el [proyecto modelo](https://gitlab.com/cencor/proyecto-modelo/):

`build.gradle`
```
...
apply from: "https://gitlab.com/cencor/governance/build-config/-/raw/main/repository/build-config-read-repo.gradle"
...
```

y para `Maven`:

```
<repositories>
  <repository>
    <id>cencor</id>
    <url>https://gitlab.com/api/v4/groups/cencor/-/packages/maven</url>
  </repository>
</repositories>
```

Más la configuración de autenticación del repositorio en el archivo `M2_HOME/conf/settings.xml`

```
...
<servers>
  <server>
    <id>cencor</id>
    <configuration>
      <httpHeaders>
        <property>
          <name>Deploy-Token</name>
          <value>REEMPLAZAR_POR_EL_TOKEN_DE_LECTURA_DE_ARTEFACTOS</value>
        </property>
      </httpHeaders>
    </configuration>
  </server>
</servers>
...
```

Donde para obtener acceso a las dependencias internas de CENCOR deberás solicitar un token de lectura del repositorio de artefactos de CENCOR.
