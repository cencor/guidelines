# Manejo de dependencias

La administración de dependencias es una característica fundamental para cualquier proyecto. Generalmente un proyecto cuenta con varias dependencias, por ejemplo Spring, JUnit, entre otras. Cada una de estas dependencias cuenta con sus propias dependencias, conocidas como dependencias transitivas.

De esta forma nuestro proyecto reutiliza mucho código a través de las dependencias que son parte del proyecto, así como de cada una de las dependencias transitivas.

Uno de los principales retos de las dependencias es manejar el conflicto entre las distintas versiones de las dependencias transitivas. Por ejemplo, si agregamos la dependencia A y la B, A teniendo como dependencia transitiva a C vesión 1 y B teniendo como dependencia transitiva a C versión 2. ¿Qué versión de C debe tomar nuestro proyecto?

## Herramienta de construcción.

Afortunadamente existen las herramientas de construcción que nos ayudan a tratar con los posibles conflictos entre las dependencias transitivas de nuestro proyecto.

Gradle es la herramienta de construcción adoptada por CENCOR que se va a encargar de administrar las dependencias del proyecto.

Maven es la segunda herramienta de construcción adoptada por CENCOR, principalmente para componentes legados.
