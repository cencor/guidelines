# Resolución a errores conocidos en Liquibase

## Error al desplegar aplicacion por que se hicieron cambios por fuera de liquibase

Este error puede ocurrir por dos causas:
1. En el archivo application.properties la propiedad ***spring.jpa.hibernate.ddl-auto*** esta con un valor incorrecto (Ej. update), cuando el correcto debe de ser ***validate***.
2. Se hizo alguna modificación a la base de datos de forma directa

Ejemplo de una columna que ya existe en la base de datos, pero que no se había ejecutado por Liquibase
```
Caused by: java.sql.SQLSyntaxErrorException: Duplicate column name 'BlockTradeType'
	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:120) ~[mysql-connector-java-8.0.29.jar!/:8.0.29]
	at com.mysql.cj.jdbc.exceptions.SQLExceptionsMapping.translateException(SQLExceptionsMapping.java:122) ~[mysql-connector-java-8.0.29.jar!/:8.0.29]
	at com.mysql.cj.jdbc.StatementImpl.executeInternal(StatementImpl.java:763) ~[mysql-connector-java-8.0.29.jar!/:8.0.29]
	at com.mysql.cj.jdbc.StatementImpl.execute(StatementImpl.java:648) ~[mysql-connector-java-8.0.29.jar!/:8.0.29]
	at liquibase.executor.jvm.JdbcExecutor$ExecuteStatementCallback.doInStatement(JdbcExecutor.java:393) ~[liquibase-core-4.10.0.jar!/:?]
	at liquibase.executor.jvm.JdbcExecutor.execute(JdbcExecutor.java:83) ~[liquibase-core-4.10.0.jar!/:?]
	at liquibase.executor.jvm.JdbcExecutor.execute(JdbcExecutor.java:151) ~[liquibase-core-4.10.0.jar!/:?]
```

Para resolver este error y asegurarnos que en un ambiente limpio se cree correctamente todo el schema como se necesita hay que seguir los siguientes pasos

1. Localizar el xml que contiene el cambio que se tiene el problema
2. Crear precondición en liquibase para que valide si existe el objeto o columna
3. En el parametro de OnFail poner el valor de ***MARK_RAN*** para que se agregue a la tabla de liquibase
4. Crear nuevo MR para que se deploye de forma automática
5. La documentación de liquibase se encuentra en el siguiente link por si se require algun tag para tabla, columna, vista, etc. [Documentación](https://docs.liquibase.com/concepts/changelogs/preconditions.html)


Ejemplo:
```
<changeSet id="19-02-08_02" author="AJHM">
        <preConditions onFail="MARK_RAN">
            <not>
                <columnExists tableName="DirectSwitchGroup" columnName="Aggressor"/>
                <columnExists tableName="DirectSwitchGroup" columnName="FinalRange"/>
                <columnExists tableName="DirectSwitchGroup" columnName="MustTrade"/>
            </not>
        </preConditions>
        <addColumn tableName="DirectSwitchGroup">
            <column name="Aggressor" type="VARCHAR(50)"/>
            <column name="FinalRange" type="BIGINT"/>
            <column name="MustTrade" type="TINYINT"/>
        </addColumn>
    </changeSet>
```

