# Propósito de un comentario

Su propósito es explicar el código que no se explica por sí mismo. Además pueden/deben advertir sobre consecuencias o decisiones tomadas.

En ocasiones es más fácil ejemplificar con lo que **no** se debe hacer. En este caso echa un vistazo a que cosas resultan en malos comentarios:

## Información inapropiada

Es inapropiado para un comentario almacenar información que es mejor almacenada en un diferente tipo de sistema como en Git, el sistema de seguimiento de incidencias (JIRA) o algún otro sistema. Historiales de revisión, por ejemplo, simplemente agregan volúmenes de texto nada interesantes.

**En general**, datos como autores, fecha de última modificación, número de incidente y demás no deben de aparecer en los comentarios. Los comentarios **deben ser exclusivamente reservados para notas técnicas acerca del código y diseño.**

## Comentario obsoleto

Un comentario que se ha hecho viejo, irrelevante o incorrecto es obsoleto. Los comentarios se vuelven obsoletos rápidamente. Es mejor no escribir un comentario que se convertirá en obsoleto. Si encuentras un comentario obsoleto, es mejor actualizarlo o borrarlo tan pronto como sea posible. Los comentarios obsoletos tienden a confundir y ser irrelevantes al código.

**Recuerda**, sólo coloca comentarios al código que no se puede explicar por sí mismo.

## Evita comentarios redundantes

Un comentario es redundante si describe algo que el código se describe por si mismo.
Por ejemplo:

```
// Si tiene 18 años o más.
if (edad >= 18) {
   ...
}
```

Otro ejemplo es un Javadoc que no dice nada adicional (en ocasiones menos que) a la firma del método:

```
/**
*
* @param sellRequest
* @return
* @throws ManagedComponentException
*/ 
public SellResponse beginSellItem(SellRequest sellRequest) throws ManagedComponentException
```

Los comentarios **deben decir cosas que el código no puede decir por sí mismo**.

## Elimina código muerto

**Código muerto** es aquel que no se ejecuta. Puede estar comentado y también se puede encontrar en el cuerpo de un if que evalúa una condición que nunca pasa. Lo encuentras en métodos de utilería que nunca son invocados o en condiciones de un switch/case que nunca ocurren. Nadie sabe que tan viejo es, ni tampoco saben si es valioso para el sistema. Peor aún, nadie lo borrará porque todos asumen que alguien más lo necesita o tiene planes para él.

El código comentado se queda ahí y le salen raíces, siendo cada vez más y más irrelevante con cada día que pasa. El problema con código muerto es que después de un rato comienza a apestar. Es como el queso, mientras más viejo es, más fuerte y amargo su olor es. Esto es porque código muerto no se actualiza completamente cuando se cambia el diseño.

Continua compilando, pero no sigue las nuevas convenciones o reglas. Fue escrito en algún momento cuando el sistema era diferente. Cuando veas código muerto, haz la mejor cosa. **Dale un entierro decente y bórralo del sistema**. No te preocupes, Git lo mantendrá. Si realmente alguien lo necesita, podrá obtenerlo de una versión anterior.