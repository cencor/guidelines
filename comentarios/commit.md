# Describe tus comentarios en cada **commit**

Alguna vez has revisado el historial de cambios de Git y pensado, ¿Qué pasó aquí?

Por ejemplo:

`fix test.`

o

`added logged out user page.`

o

`changes`

¿Qué se supone que debo de hacer con eso? Esto no me dice nada de lo que está sucediendo en el proyecto. Si veo los comentarios seis meses a partir de ahora no tendría ni idea a qué prueba se refiere fix test o a que página se refiere user page. De hecho, justo ahora no tengo la capacidad para descubrir el significado.

El mensaje de commit **describe los cambios que se han agregado al proyecto** y ayudan al usuario a entender la historia de los archivos contenidos en el repositorio. Ergo, este mensaje debe ser descriptivo e informativo sin repetir los cambios en el código.

Existen tres cosas básicas que se deben de incluir:

* **Título o resumen. Requerido** El titulo debe estar en **imperativo**. Por ejemplo, `“Corrige formato ...”` o `Valida el tamaño del nombre de la institución` en lugar de `Se corrigió formato ...` o `Se agrega ...` o `Corrección de ...` o `Se atienden comentarios`. Pues **intentamos describir lo que hace el commit**, no lo que se hizo.
* **Descripción detallada. Opcional** Describe el por qué del cambio, cómo se soluciona y/o los efectos secundarios si es que existen.
* **Número de seguimiento. Requerido si existe.** Id de incidente (Jira, Gitlab).

Cada uno de los elementos mencionados deben ir separados por una línea en blanco.

Por ejemplo:

```
Corrige error donde el usuario no puede registrarse en el sistema.

Los usuarios no podían registrarse al sistema si no habían visitado antes la
página de precios porque se esperaba que existiera la información de 
seguimiento, misma que se envía al log después de que el usuario se registra.

Se corrige agregando una validación para asegurar que si la información de 
seguimiento no existe que no se intente escribir en el log.

https://cencor.atlassian.net/browse/PRY-492
```

o un ejemplo donde se limpia código y no hay un Jira asociado.

```
Elimina constante no utilizada
```

o un ejemplo donde agregar una descripción no agrega valor pues se puede encontrar el detalle en el incidente asociado.

```
Genera reportes vacíos en vez de arrojar excepción

https://cencor.atlassian.net/browse/PRY-492
```