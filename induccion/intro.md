# Inducción

## Recursos Humanos

## Entendimiento del negocio

## Entendimiento del ciclo de desarrollo de software

### Repositorio de control de versiones

### Testing

### Domain Driven Design

### Integración continua

### Despliegues

## Primeras actividades

### Configuración de ambiente

#### Linux

#### Llave publica SSH

#### Gitlab

#### Java

Instalacion de Java 8, 11 y 17 de Amazon Corretto

#### Gradle

Instalación de Gradle 7 o superior

#### Construcción de componentes legados

##### Maven

Instalación de Maven 3.6.3

##### Ant

Instalación de Ant 1.6.5

#### NPM

Instalacion de:

* NPM
* NVM Necesario para gestionar multiples versiones de NPM.

#### IDE

##### Back end

Alguno de los siguientes IDEs.

* IntelliJ **Recomendado**
* Eclipse
* Algun otro de tu preferencia

##### Front end

Alguno de los siguientes IDEs.

* Visual Studio Code **Recomendado**
* IntelliJ
* Algun otro de tu preferencia

#### Repositorio de artefactos

#### Postman

#### Correo electrónico

#### Calendarios

#### Microsoft Teams

##### Canales

#### Jira

#### Confluence

#### Docker

##### Scripts y hacks

* Liberación de espacio

#### SonarCloud

#### Dependency Track

#### Docker

#### Bóveda

#### Extras

* Sublime Text editor

##### Mac

* [Spectacle](https://www.spectacleapp.com) Gestor de ventanas en Mac. Tamaño completo, media pantalla, etc.

##### Linux