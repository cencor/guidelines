Una de las deciciones más importantes de arquitectura en Java es cómo utilizar las excepciones. Se trata de poder pensar en la naturaleza de las excepciones y patrones de diseño que nos ayudaran con nuestro diseño. Permitiendo tener al mismo tiempo código limpio y un buen manejo de excepciones que nos traerán grandes beneficios si las utilizamos correctamente.

# Excepciones asociadas a un error del cliente

A continuación se presentan algunas excepciones que pueden ser utilizadas para asociarlas con un error de un cliente. Ya sea porque están incumpliendo alguna regla de negocio o que intentan llevar a cabo una modificación de la aplicación en un estado inválido. Por ejemplo:

* **IllegalArgumentException** Para indicar que la información enviada por el cliente es inválida, ya sea por temas de formato, tipo de dato o que se incumple alguna regla de negocio. En HTTP sería el equivalente a un `400`
* **IllegalStateException** Para indicar que la operación que el cliente desea ejecutar no se puede llevar a cabo por que el sistema está en un estado distinto. Por ejemplo si la operación es cancelar un pedido se realiza cuando el pedido ya está en reparto. En HTTP sería el equivalente a un `400`
* **NoSuchElementException** Para indicar que el(los) elemento(s) que se desea buscar no existe de acuerdo a las condiciones que el cliente solicitó. En HTTP sería el equivalente a un `404`
* **Cualquier excepción presonalizada que extienda alguna RuntimeException de las anteriores** Si se quiere representar el lenguaje ubicuo para indicar algún error de negocio podemos implementar alguna excepción personalizada. En HTTP sería el equivalente a cualquier error `400`. Por ejemplo: 

`DeskNotFoundException extends NoSuchElementException` Cuando no existe una mesa con las condiciones indicadas.

`InvalidOrderTypeException extends IllegalArgumentException` Cuando el tipo de orden es inválido.

`TradeAlreadySettledException extends IllegalStateException` Cuando el trade ya había sido liquidado y se intentaba realizar alguna operación antes de la liquidación.

# Excepciones asociadas a un error interno

Por otro lado existen ocasiones donde podemos tener algún error interno. Este podría ser provocado por alguna condición de carrera o simplemente que no pudimos escribir un archivo porque se terminó el espacio disponible en el servidor. Para estos casos se recomienda utilizar alguna de las siguientes excepciones:

* **Excepción personalizada que extienda RuntimeException** En caso de querer representar el lenguaje ubicuo dentro de la excepción podemos crear una excepción personalizada de tipo Runtime para estos fines. Equivalente a cualquier HTTP `500`. Por ejemplo, si dependemos de un servicio REST para llevar a cabo nuestra tarea y este no esta disponible, podríamos crear una `BoeNotAvailableException` que sería equivalente a un HTTP `503`
* **RuntimeException** Para representar algún error interno de la aplicación del cual no nos podemos recuperar. Equivalente a un HTTP `500`

# Incluye información suficiente de la falla en el mensaje de la excepción

Es importantísimo que el mensaje de la excepción muestre la mayor cantidad de información relacionada a la causa de la falla. Es de tal magnitud que la diferencia entre tener la información suficiente o no puede ser de varias horas de diagnóstico y noches de desvelo tratando de identificar por qué fue la falla.En otras palabras, el nivel de detalle en el mensaje de la excepción debe inluir la razón de la falla para poder entender la falla y llevar a cabo un análisis. 

Solo imagina, ponte del lado de quien recibe la excepción. Podría ser un usuario final viendo el mensaje de la excepción en un recuadro del aplicación web/móvil, o podría ser algún ingeniero de soporte que haya recibido una alerta de la falla. Piensa qué información podrías necesitar para entender que es lo que ha pasado tan solo con leer la excepción dentro del log de la aplicación o en una herramienta de monitoreo. 

Imagina que un usuario del sistema desea generar el reporte de todos los clientes al final del mes y obtiene una excepción que dice algo como

```
Error al exportar
```

El usuario sabe que hay un error pero no sabe que hacer. Por lo que te pide ayuda. Revisas el log, ubicas la excepción y te comienzas a rascar la cabeza pues no tienes idea de por qué falló. Seguro te ha pasado.

Por ejemplo, puede contener unformación de los parámetros utilizados como filtros para obtener información de algún repositorio como una base de datos.

```
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EquitySerieDetailNotFoundException extends NoSuchElementException {

    private final String type;

    private final String ticker;

    private final String serie;

    public EquitySerieDetailNotFoundException(final String instrumentType, final String ticker, final String serieId) {
        this.type = instrumentType;
        this.ticker = ticker;
        this.serie = serieId;
    }
        
    @Override
    public String getMessage() {
        return "No existe detalle de la serie para el instrumento:[ "
                + "instrumentType: " + this.type
                + ", Ticker: " + this.ticker
                + ", Serie: " + this.serie + "]";
    }
}
```

O la versión más sencilla donde simplemente llamamos al constructor padre:

```
public class EquitySerieDetailNotFoundException extends NoSuchElementException {
    
    public EquitySerieDetailNotFoundException(final String instrumentType, final String ticker, final String serieId) {
        super("No existe detalle de la serie para el instrumento:[ "
                + "instrumentType: " + this.type
                + ", Ticker: " + this.ticker
                + ", Serie: " + this.serie + "]");
    }
}
```

De esta forma al analizar el `stacktrace` podemos darnos cuenta con facilidad qué instrumento no existe en la Base de datos.

## Causa raíz

Adicional al mensaje es de mucha utilidad saber cual fue la excepción raíz, si es que hay alguna. Por ejemplo, la siguiente excepción indica que no se pudo generar un archivo `zip`. En este caso si no incluímos la excepción raíz, aunque hayamos agregado la mayor cantidad de información en el mensaje de la excepción, **sin la excepción original nunca podremos saber la falla raíz**. Si la falla fue porque no había espacio disponible en el sistema de archivos o porque hay un segmento erróneo del disco duro o porque no tenemos permisos para escribir en el directorio o miles de otras causas más:

```
try {
    ...
} catch (IOException e) {
    throw new InternalError("No se pudo generar el archivo zip del cliente " + cliente 
                + " entre " + fechaInicial 
                + " y " + fechaFinal, e);
                                      ^----- INCLUYE SIEMPRE LA CAUSA RAÍZ
}
```

En este caso al incluir la excepción raíz podemos determinar rápidamente si fue ocasionado por falta de permisos, falta de espacio o cualquiera que fuese la razón original de la falla.