# Falla inmediatamente

Las excepciones deben ser alnzadas tan pronto una condición de error se ha cumplido. Esto ayuda a encontrar fácilmente la ubicación exacta de la falla cuando se analiza el `stack trace`. Por ejemplo:

```
public void checkOut() {
   ...
   Item selectedItem = this.getSelectedItem(operationType);
   if (selectedItem == null) {
       Notifier.notifyError();
   } else {
       ...
   }
}

public Item getSelectedItem(int operationType) {
   if (...) {
       return null;
   } else if (...) {
       return null;
   } else {
       ...
   }
   return item;
}


public static void notifyError() {
   ...
   throw new ItemNotFoundException("Item does not exist or is out of stock");
}
```

El método `getSelectedItem` tiene varios puntos de falla. Aunque solo existe un solo lugar donde los distintos errores son notificados, como no se falló rápidamente, encontrar la causa original de la falla resulta en ser una adivinanza. 

Fue en el primer `if` o en el `else if` o en el `else`? No es posible saber dónde exactamente ocurrió el error.

Es **mucho mejor lanzar la excepción justo donde detectamos la falla** y no hacerlo algunas líneas o métodos después.
