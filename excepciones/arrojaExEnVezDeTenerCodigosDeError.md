# Arroja excepciones en vez de devolver códigos de error.

En el pasado existían muchos lenguajes de programación que no tenían un modelo de excepciones. Aquellos lenguajes estaban muy limitados para tratar los errores. Entonces los programadores fueron obligados a hacer algún tipo de manejo de errores. Se les ocurrió entonces los famosísisisimos códigos de error:

```
public classDeviceController {
. . .

public void sendShutDown() {
   DeviceHandle handle = getHandle(DEV1);
   // Check the state of the device
   if (handle != DeviceHandle.INVALID) {
       // Save the device status to the record field
       retrieveDeviceRecord(handle);
       // If not suspended, shutdown
        if (record.getStatus() != DEVICE_SUSPENDED) {
                pauseDevice(handle);
             clearDeviceWorkQueue(handle);
             closeDevice(handle);
       } else {
             logger.log("Device suspended. Unable to shutdown");
       } 
    } else {
           logger.log("Invalid handle for: " + DEV1.toString());
    } 
}
```

El problema con este enfoque es que el cliente (quien nos manda a llamar) tiene que revisar si existe algún error inmediatamente después de mandar a llamar el método `getHandle()`. Desafortunadamente no existe nada que nos obligue a leer el código de error/resultado. Es muy fácil olvidarse de revisar si la operación fue exitosa. 

Por esta razón, un mejor enfoque es arrojar una excepción cuando ocurre algún error. Además resulta que el código es más limpio y la lógica no se encuentra opacada por el manejo de errores:

```
public void sendShutDown() {
    try {
        tryToShutDown();
    } catch (DeviceShutDownError e) {
        // Manejo de la excepción
        ...
    }
}

private void tryToShutDown ( ) throws DeviceShutDownError {
   DeviceHandle handle = getHandle(DEV1);
   DeviceRecord record = retrieveDeviceRecord(handle);
   pauseDevice(handle);
   clearDeviceWorkQueue(handle);
   closeDevice(handle);
}


private DeviceHandle getHandle(DeviceID id) {
    ...
    throw new DeviceShutDownError("Invalid handle for: " + id.toString());
    ...
}
```

Fíjate como se hace más claro el código. No solo es por estética. El código es mucho mejor porque dos distintas responsabilidadees que fueron acopladas (el algoritmo de apagado y manejo de errores) ahora están separadas. Puedes ver cada una de esas responsabilidades y entenderlas por separado.