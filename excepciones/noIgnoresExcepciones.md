# No ignores las excepciones

Es muy fácil ignorar excepciones con tan solo ponerlas dentro de un `try` y un bloque `catch` vacío:

```
public void persist(Employee e) {
   ...
   try {
       if (1 / (e.getA() + e.getB()) {
         ...
       }
       persistenceManager.save(e);
   } catch (Exception e) {
       System.err.println(e.getMessage());
   }
}
```

Este metodo persiste la entidad empleado si la condición `e.getA() + e.getB()` es distinto de cero y que no existan excepciones al persistir la entidad dentro del recurso (Base de datos). Si alguno de estos eventos ocurren, el mensaje de la excepción se va a imprimir en la consola y la ejecución del método será exitosa. Indicándole al cliente que la entidad fue persistida, lo cual no es cierto.

Un bloque `catch` vacío simplemente arruina el propósito de las excepciones, que es obligarte a manejar las condiciones excepcionales. En casos donde nos vamos a recuperar de la excepción, como mínimo el bloque `catch` debería contener un comentario explicando porque es apropiado ignorar la excepción y dejar la aplicación en un estado consistente.
