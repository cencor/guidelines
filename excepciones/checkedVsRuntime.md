# Utiliza `checked-exceptions` para condiciones recuperables y `runtime exceptions` para todo lo demás

La principal razón de esto es mantener nuestro código con un acoplamiento bajo, ligero, flexible. Las `Checked-exceptions` obligan al cliente (quien llama el método) ya sea a cachar la excepción o relanzarla. Por ejemplo:

```
public E take() throws InterruptedException {
   ...
}
```

y el cliente

```
try {
    event = linkedBlockingQueue.take();
} catch (InterruptedException e) {
    ...
}
```

En este caso el cliente tiene que tratar la `InterruptedException`, que puede ocurrir cuando otro hilo despierta a este hilo que se encontraba esperando a que llegara un mensaje de la `queue`. Dependiendo del comportamiento del sistema podríamos considerar que la `InterruptedException` es una señal para detener el consumo de mensajes, manejar la excepción de acuerdo a lo que se necesite o simplemente volver a llamar el método `take` para continuar con la recepción de mensajes. En este caso nosotros podemos tomar el control de la excepción y decidir como recuperar el sistema de la situación excepcional.

Si este no es el caso y nosotros arrojamos una `checked exception`, todas las capas superiores de nosotros que no saben que hacer en este caso excepcional tendrán que declarar la excepción en su firma del método. ¿Cuál es la consecuencia? Se genera un acoplamiento muy fuerte además de que se comienza a ensuciar nuestro código. Imagina que quisieras cambiar el nombre de la excepción para representar el lenguaje ubicuo en la excepción. Hacerlo implica que todos los métodos que la tienen definida en su firma del método, tendrán que hacerlo. Inclusive si no les importa el cambio en el nombre.

## `Checked-exceptions` de un API que uso

Algunas veces existen casos donde llamas un método de una libería externa, o un API externo, del cual no tienes control, que arroja una `checked exception`. Puede ser que no sepas que hacer con ese tipo de excepción, quizás no te puedas recuperar de la misma. Piensas que debió ser una `Runtime exception`. En esos casos lo que puedes hacer es encapsular esa `checked exception` dentro de una `runtime exception`:

```
public class TradeListenerAdapter {

    public Person findById(final String jsonMessage) {
        ...
        try {
            Trade trade = mapper.readValue(jsonMessage, TradeCreatedEvent.class);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Se ha recibido un evento de creacion de trade invalido: " 
                + jsonMessage, e);
        }
        ....
    }
}
```

## En resumen

Si crees que la excepción permite que exista algún mecanismo de recuperación, entonces utiliza una `checked exception`; si no, usa una `runtime exception`. Si no es tan fácil determinar si una recuperación es posible, definitivamente quedaría mejor que uses una `runtime exception`
