# Short-lived branches automation

A continuación se muestran los pasos para poder crear branches de corta duración, posteriormente generar un Pull Request y hasta que este branch de corta duración haya sido integrado en master.

1. Actualizar master:

```
git checkout master
git pull
```

2. Crear un branch de acuerdo a la funcionalidad a agregar:

```
git checkout -b prefijo-nombre-de-la-funcionalidad
```

o

```
git checkout -b prefijo-NUMERO-Jira
```

Donde prefijo puede tomar uno de los siguientes valores:

* **major**: Utilizado para todos aquellos cambios que hagan la aplicación incompatible con la versión anterior. Por ejemplo cambio de versión de framework de spring boot 1 a spring boot 2, eliminación de alguna operación REST.

```
git checkout -b major-framework-upgrade
```

* **feature o minor**: Cuando agregamos funcionalidad que permita a nuestro componente ser compatible con versiones anteriores. Por ejemplo agregar una nueva operación REST o mejorar un algoritmo de búsqueda que no afecte el comportamiento funcional actual.

```
git checkout -b feature-save-interest-rate
git checkout -b minor-expose-save-interest-rate-on-rest
```

* **patch, fix, hotfix o doc**: Cuando corregimos bugs que son compatibles con versiones anteriores. Por ejemplo, mejorar el uso de log, corregir algún bug, agregar documentación del componente.

```
git checkout -b fix-rate-calc
git checkout -b hotfix-ABC-123
git checkout -b doc-installation-process
git checkout -b patch-interest-rate-on-marketclose
```

3. Agregar los cambios en código y generar uno o varios commits

```
git add file1
git commit -m "Guarda tasa de interes"
git add file2
git add file3
git commit -m "Expone guardado de tasa de interes por REST"
```

4. Subir los cambios al branch remoto

```
git push --set-upstream origin major-springboot2upgrade
```

5. A través de un navegador ingresar al repositorio en Github y crear un Pull Request.
	1. La creación del Pull Request requiere agregar al menos a un revisor y aprobador de los cambios. Asegura que incluyes al menos a un experto de ese componente.
	2. Tras crear el Pull Request, nuestro servicio de integración continua (CI) construirá este componente sobre nuestro branch.
		* En caso de algún error reportado por el servicio de CI repetir los pasos 3 y 4.
		* No es necesario crear un nuevo Pull Request ya que nuestros cambios se registran ahí mismo.

6. Atender todos los comentarios derivados de las revisiones que hicieron las personas que seleccionamos
	* Esto puede resultar en generar cambios. Bastará con repetir los pasos 3 y 4.
	* No es necesario crear un nuevo Pull Request ya que nuestros cambios se registran ahí mismo.

7. La persona que es aprobador integrará nuestro branch en master.

8. Esperar a nuestro servicio de CI nos indique que todo haya terminado correctamente
	* En caso de haber algún problema detectado por nuestro servicio de integración continua tras la integración de nuestros cambios en master, volver a repetir desde el paso 1 para realizar la corrección y dejar master en verde.