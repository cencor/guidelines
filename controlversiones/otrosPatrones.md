# Otros patrones de uso de control de versiones

A continuación se enumeran otros patrones de uso de control de versiones solamente para su conocimiento. Recordamos que el patron de uso de control de versiones seleccionado para CENCOR es short-lived branches.

## Trunk-based o Desarrollo sobre la línea base, conocido como master/main(Git)

Este método de desarrollo es regularmente pasado por alto. Aunque es una forma sumamente efectiva para desarrollar y de las escasas formas que habilitan la integración continua.

En este patrón los desarrolladores casi siempre hacen push/check-in hacia master. Los branches son raramente usados. Los beneficios de desarrollar sobre master incluyen:

* Asegurar que todo el código es integrado continuamente
* Asegurar que los desarrolladores obtengan los cambios de los demás inmediatamente
* Evitar los famosos "merge hell" e "integration hell" al final del proyecto.

En este patrón los desarrolladores trabajan sobre master, subiendo (committing/pushing) código al menos una vez al día. Cuándo se enfrentan a la necesidad de hacer un cambio complejo, como nueva funcionalidad, refactorización de alguna parte del sistema, ejecutando mejoras de largo alcance o redistribuyendo las capas del sistema no se utilizan branches. En su lugar, los cambios de planean e implementan como un conjunto de pasos pequeños e incrementales que mantengan las pruebas en verde asegurando que no se afecta funcionalidad existente.

¿Cómo lo hacen? La respuesta es una buena **componetización del software, desarrollo incremental** y **ocultar funcionalidad (feature hiding)**. Esto requiere mas cuidado en la arquitectura y desarrollo pero los beneficios de no tener una fase impredecible de integración al final del proyecto, donde múltiples branches/streams deban ser integrados para crear un branch de liberación (release), claramente tiene un mayor peso.

### Haciendo cambios complejos sin branches

En una situación donde se necesite realizar un cambio complejo al código, el crear un branch sobre el cual realizas tus cambios de tal forma que no interrumpes a otros desarrolladores aparenta ser la acción más sencilla. Sin embargo, en la practica, este enfoque conlleva a tener múltiples branches de larga duración que difieren substancialmente uno del otro y de master. Integrar branches cercano a la liberación es casi siempre un proceso complejo que toma una cantidad tiempo impredecible. Cada integración ocasiona que se descomponga funcionalidad existente y es seguido de un proceso de estabilización de master antes de que la siguiente integración ocurra.

De hecho, te enfrentas a todos los problemas que la integración continua se supone resuelve. **Crear branches de larga duración va totalmente en dirección contraria a una estrategia de integración continua**. Yendo de A hacia B vía cambios incrementales que regularmente son enviados (pushed/checked in) hacia master es casi siempre lo correcto. Así que coloca este patrón por encima de otras opciones.

## Branch for release

La situación donde a pocos días antes de la liberación se genera un branch excluivo para la liberación. Una vez creado este branch se ejecutan las pruebas y validaciones sobre el código del branch al mismo tiempo que nuevos desarrollos se continúan realizando sobre master.

El crear un branch para la liberación remplaza la mala práctica de congelación de código, en donde agregar nuevos cambios (check-in/push) se prohíbe por completo varios días y en ocasiones semanas. Al crear este branch los desarrolladores continúan realizando cambios sobre master mientras que se realizan cambios al branch de liberación para corregir bugs asociados con la liberación.

En este patrón:

* La funcionalidad es realizada sobre master
* Un branch se crea cuando el código tenga la funcionalidad completa para una liberación en particular sin interrumpir la adición de nueva funcionalidad.
* Únicamente se envían correcciones al branch de liberación y se integran inmediatamente con master.
* Cuando se ejecuta la liberación, este branch se etiqueta. Este paso obligatorio si el CVS solamente maneja cambios por archivo, como CVS, StarTeam o ClearCase.

Este estilo de branching no trabaja muy bien en proyectos grandes porque es difícil para equipos grandes o múltiples equipos terminar un trabajo simultaneamente. En este caso, es ideal tener una arquitectura componetizada con un branch de liberación (release) por componente. De este modo los equipos que ya terminaron pueden seguir con nueva funcionalidad mientras el resto termina su trabajo.

Es muy importante en este patrón no crear más branches a partir de un branch de liberación. Los branches para las siguientes liberaciones deben ser tomados desde master. Crear branches a partir de branches de liberación crea una estructura de escalera lo cual hace muy difícil saber que código es común entre los distintos branches.

Ejecutando este patrón continuamente o con cierta frecuencia (una vez a la semana) no tiene sentido crear un branch para una liberación. En estos escenarios es más fácil y barato simplemente poner una versión nueva del software que hacer un parche.

## Branch by feature

Este patrón está diseñado para hacer más fácil el trabajo simultaneo en equipos grandes al estar agregando nueva funcionalidad al mismo tiempo que mantener master (mainline) en un estado liberable (releasable). Cada historia o funcionalidad es desarrollada en un nuevo branch por separado. Solamente cuando la funcionalidad es probada y validada es cuando se integra a master para asegurar que master siempre esté liberable.

Existen algunos prerrequisitos para que este patrón funcione, menos que bien:

* Cualquier cambio en master debe ser integrado inmediatamente en cada branch diariamente.
* Los branches deben ser de corta duración, idealmente menos de un par de días.
* El número de branches activos que existan en un momento debe ser limitado al número de historias en ejecución
* Considerar tener el visto bueno de las historias antes de integrarlas a master
* Las refactorizaciones deben ser integradas inmediatamente en master para minimizar conflictos de integración

Tener muchos branches de larga duración es malo porque existe el problema combinatorio al integrar. Si tienes cuatro branches cada uno de ellos diverge. Solamente toma dos branches trabajando una refactorización de código para llevar a todo el equipo a detenerse por los conflictos resultantes. Vale la pena mencionar que esta técnica va en **dirección opuesta** a la integración continua.