# Control de versiones en Git

Los sistemas de control de versiones, también conocidos como sistemas de control de código o sistemas de control de revisiones, están diseñados para permitir a las empresas mantener una historia completa de cada cambio hecho a sus aplicaciones, incluyendo el código, documentación, definiciones de BD, scripts de construcción, pruebas y demás. Sin embargo los sistemas de control de versioens también sirven para otro importantísimo propósito: **Habilitan a los equipos a trabajar juntos como equipo** en distintas partes de la aplicación al mismo tiempo de mantener un registro del sistema, la base de código definitiva de una aplicación.

Una vez que el equipo va incorporando más desarrolladores, se vuelve más difícil tener muchas personas trabajando de tiempo completo en el mismo repositorio de control de versiones. Las personas pueden por error descomponer una funcionalidad y generalmente estar planchando o sobreescribiendo los cambios de los demás. El objetivo de esta sección es examinar como los equipos pueden trabajar productivamente con el control de versiones.

## Patron de uso del control de versiones en Git para todos los desarrollos en CENCOR.

Existen dos razones por las cuales el uso del control de versiones es sumamente importante. Primero, los patrones de control de versiones son el eje central de un pipeline de despliegue. Segundo, las practicas pobres del uso de control de versiones son una de las principales barreras para liberaciones rápidas y de bajo riesgo.

Existe una tensión entre el deseo de integración continua y el deseo de usar branches. Por un lado, una vista absoluta de integración continua dice que cada cambio debe ser integrado tan pronto como sea posible a master. Master es siempre el estado más actualizado y completo de la aplicación. Mientras más tiempo permanezcan los cambios separados de master existe un mayor riesgo de que exista un problema tras integrar los cambios.

Dicho esto **no** se utilizarán los patrones branch by feature, branch for release y todos aquellos patrones que mantengan cambios fuera de master por mucho tiempo. Los desarrollos en CENCOR utilizarán el patrón short-lived branches que se integran frecuentemente a master a través de un Pull Request.

### Patrón Short-lived Branch

Los sistemas de control de versiones distribuídos (DVCS) están diseñados exactamente para este patrón y hacen absolutamente fácil la integración desde y hacia master. Cada cambio, no necesariamente una funcionalidad, es desarrollada en un nuevo branch. El tiempo de vida de un branch en este patrón es menor a un día. Tan pronto esté finalizado el cambio es solicitada una inspección a un integrante del equipo quien tendrá el poder de aceptar el cambio e integrarlo en master o de rechazarlo si es que existiera algún riesgo de romper el estado de liberable de master.

Existen algunos prerrequisitos para hacer que este patrón funcione:

* Los branches deben ser ultracortos en duración, menos de un día. Idealmente no más de cuatro de horas.
* Branches nuevos deben de salir a partir de master, no de algún otro branch.
* Mantener siempre master en un estado liberable.
* Aplica para equipos pequeños de desarrollo (Max 8 personas por repositorio)
* Las validaciones y pruebas se realizan sobre master, no sobre un branch. Corrigiendo tan pronto posible algún defecto tras la integración.
* El equipo está completamente de acuerdo en integrar con master con mucha frecuencia.
* Es necesario combinar las estrategias de funcionalidad oculta y desarrollo incremental.
* Un branch no necesariamente está asociado a una funcionalidad sino a una parte de la misma.

Este patrón debe ser usado con mucho cuidado evitando tener branches de larga duración pues nuevamente caeríamos en un esquema que va en dirección opuesta a la integración continua. La única manera de obtener todos los beneficios de trabajar en un branch e inspeccionar los cambios a incluir en master es integrando continuamente cada uno de los branches con una alta frecuencia. Menos de un día e idealmente en alrededor de un par de horas.

La principal ventaja de este patrón es que el conocimiento de la aplicación se va difundiendo con cada inspección. Los integrantes del equipo pueden aprender de otros así como prevenir y defender el código. Además de prevenir agregar código a master que pueda afectar el comportamiento de la aplicación. Por último nos ayuda a evaluar cada Pull Request con herramientas de calidad y estilo de código, como sonarqube y checkstyle, para verificar la calidad de nuestros desarrollos.