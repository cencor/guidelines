# Resolución a errores conocidos en Gitlab

## Error al obtener el tag de la última versión para autoversionado

A veces, cuando aceptamos dos mr caasi al mismo tiempo, al momento de hacer el autoversionado y crear el nuevo tag, 
se presenta un error ya que no puede identificar el último tag registrado. Presentando algo así como se observa a 
continuación, en ese caso, el tag con la versión 1.4.0 ya existía pero no se tomaba como último.

```
Obteniendo version actual del ultimo tag creado. En su defecto de la version indicada en gradle.properties
Versión actual -> 1.3.1
minor change
Versión a generar -> 1.4.0
HEAD detached at 9dc363a
Untracked files:
(use "git add <file>..." to include in what will be committed)
gradle.propertiese
nothing added to commit but untracked files present (use "git add" to track)
Cleaning up file based variables
00:01
ERROR: Job failed: exit code 1
```

Para resolver este error, seguir los siguientes pasos:
1. Entrar a GitLab con el usuario code.squad.cencor.
2. Nos dirigimos al proyecto donde se presenta el error.
3. Vamos a menú izquierdo: ***Repository > Tags***.
   - Otra opción de ir a menú izquierdo: ***Repository > Graph***
   - Ir hacia el úlgimo tag y a partir de este, crear el nuevo (***Options > Tag***)
4. Creamos un nuevo tag.
5. Una vez creado el nuevo tag, podemos ir a el job donde se presenta el error y dar ***Retry***.

## Error en ejecución de una tarea programada (Schedules)

Las tareas programadas dentro de gitLab se recomiendan realizarlas con el usuario @code.squad.cencor, ya que en caso 
de cualquier cambio de permisos sobre tu usuario o salida del proyecto, se asegura la ejecución de las tareas programadas,
el usuario @code.squad.cencor se mantiene estático y con fines administrativos.

Pasos para generar una tarea programada:
1. Entrar a GitLab con el usuario code.squad.cencor.
2. Nos dirigimos al proyecto donde se requiere programar la tarea.
3. Vamos a menú izquierdo: ***CI/CD > Schedules***.
4. Crear la tarea:
   - Ir a ***New schedule***.
   - Si la tarea ya está generada, tomar el control ***Take ownership***. 