# Evita poner en múltiples entradas un mismo evento. Por ejemplo:

```
logger.ERROR("Imposible consultar póliza");
logger.ERROR("Id de cliente: " + cliendId);
logger.ERROR("póliza: " + policyId);
```

Esto hará más complejo rastrear el error pues todos los logs se centralizarán y podrían quedar mezclados

```
String errorDesc = "Imposible consultar póliza:\n"
errorDesc += "Id de cliente: " + clientId;
errorDesc += "póliza " + policyId;
logger.ERROR(errorDesc);
```