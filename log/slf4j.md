# SLF4J + Log4J2

SLF4J como interface, Log4J2 como implementación.

SLF4J es un framework que abstrae la tecnología con la que se desea generar los logs. Cuenta con capacidades de generar logs con Log4j, Log4J2, Logback, entre otros. Hacerlo de esta manera permite cambiar con gran facilidad la herramienta responsable de generar los logs. Por lo que si en algún momento logback conviene en vez de log4j2 simplemente tendríamos que hacer un cambio en las dependencias sin mover una sóla línea de código.

```
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
@EnableJpaRepositories
public class NotifierMsApplication {
        /***/
        private static final Logger LOGGER = LoggerFactory.getLogger(NotifierMsApplication.class);
        /**
         * @param args Esta aplicación no requiere de parámetros al inicializar.
         */
        public static void main(final String[] args) {
                SpringApplication.run(NotifierMsApplication.class, args);
                LOGGER.info("Inicializado correctamente");
        }
}
```

## Configuración de ejemplo:

```
<?xml version="1.0" encoding="UTF-8"?>
<Configuration name="log-config">
    <Properties>
        <Property name="log-level">${env:LOG_LEVEL:-INFO}</Property>>
        <Property name="log-path">${env:LOG_FILE_PATH:-/tmp}</Property>>
        <Property name="log-file-name">${env:LOG_FILE_NAME:-application}</Property>
    </Properties>
    <Appenders>
        <RollingFile name="RollingFile_Appender" fileName="${log-path}/${log-file-name}.log" filePattern="${log-path}/${log-file-name}-%d{yyyy-MM-dd-HH-mm-dd}.log">
            <PatternLayout>
                <Pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%-5level] [%t] %c{1.} - %msg%n</Pattern>
            </PatternLayout>
            <Policies>
                <SizeBasedTriggeringPolicy size="10 MB"/>
            </Policies>
        </RollingFile>
    </Appenders>
    <Loggers>
        <Root level="${log-level}">
            <AppenderRef ref="RollingFile_Appender"/>
        </Root>
        <Logger name="com.cencor" level="${log-level}" additivity="false">
            <AppenderRef ref="RollingFile_Appender"/>
        </Logger>
        <Logger name="org.springframework" level="${log-level}" additivity="false">
            <AppenderRef ref="RollingFile_Appender"/>
        </Logger>
    </Loggers>
</Configuration>
```

## Dependencias

SLF4J solo requiere de agregar al classpath las siguientes librerías así como el archivo de configuración para automáticamente generar los logs.

```
compile group: 'org.springframework.boot', name: 'spring-boot-starter-log4j2', version: '2.7.1'
```