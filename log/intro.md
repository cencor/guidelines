El manejo de logs es la parte central de la estrategia de monitoreo. Sistemas operativos y middleware (Application servers, JMS brokers, etc.) producen logs que son tremendamente utilies para entender el comportamiento del usuario y para poder rastrear el origen de los problemas.

Las aplicaciones también deben de producir logs de buena calidad. En particular es muy importante poner atención a los niveles de log, como DEBUG, INFO, WARNING, ERROR y FATAL.

Por default las aplicaciones deben mostrar únicamente los niveles WARNING, ERROR y FATAL pero configurable en tiempo de ejecución para mostrar otros niveles si fuera necesario.

## Nivel de log

### DEBUG

Eventos que brinden la información necesaria para poder diagnosticar y/o depurar. Por ejemplo, incluir el cuerpo del request recibido. Otro uso es mostrar errores de aplicación recuperables tales como un usuario que no pudo iniciar sesión o información de rastreo.

### INFO

Información para entender el comportamiento de una petición, eventos que tengan un significado de negocio o mensajes de progreso. Por ejemplo orden de compra colocada, calculando tarifa, cargando TDC, etc.

### WARNING

Para proveer alertas de que algo está mal, quizas inesperado, o que se utilizó un workaround. Por ejemplo alguna excepción cachada y que tenemos un mecanismo de recuperación automático. En fin cualquier evento que suceda en la aplicación pero que aún nos permita continuar.

```
2017-05-04 09:04:22 WARN  [http-nio-9117-exec-1] org.hibernate.engine.jdbc.spi.SqlExceptionHelper : SQL Warning Code: 4474, SQLState: 01000

2017-05-04 09:04:22 WARN  [http-nio-9117-exec-1] org.hibernate.engine.jdbc.spi.SqlExceptionHelper : [jcc][t4][10217][10310][4.21.29] Connection read-only mode is not enforceable after the connection has been established.
```

### ERROR y FATAL

Para algún error del cual no nos podamos recuperar automáticamente.

Por ejemplo, si algún servicio externo excede su tiempo de respuesta (time-out) o si es que no podemos seguir procesando transacciones si el filesystem está lleno.

```
2017-05-05 09:57:40 ERROR org.apache.coyote.http11.Http11NioProtocol : Failed to start end point associated with ProtocolHandler [http-nio-8080]
java.net.BindException: La dirección ya se está usando
        at sun.nio.ch.Net.bind0(Native Method)
        at sun.nio.ch.Net.bind(Net.java:433)
        at sun.nio.ch.Net.bind(Net.java:425)
        at sun.nio.ch.ServerSocketChannelImpl.bind(ServerSocketChannelImpl.java:223)
        at sun.nio.ch.ServerSocketAdaptor.bind(ServerSocketAdaptor.java:74)
        at org.apache.tomcat.util.net.NioEndpoint.bind(NioEndpoint.java:228)
        at org.apache.tomcat.util.net.AbstractEndpoint.start(AbstractEndpoint.java:874)
        at org.apache.coyote.AbstractProtocol.start(AbstractProtocol.java:590)
        at org.apache.catalina.connector.Connector.startInternal(Connector.java:969)
        at org.apache.catalina.util.LifecycleBase.start(LifecycleBase.java:150)
        at org.apache.catalina.core.StandardService.addConnector(StandardService.java:225)
        at ...
```

## De primera clase

El logging debe ser tratado como requerimiento de primer nivel, al mismo nivel que otros requerimientos no funcionales.