# Layout

* Timestamp. Fecha y hora en que ocurrió el evento.
* Nivel de log. INFO, WARN, DEBUG, ERROR o FATAL
* Nombre del componente. El nombre del componente donde se originó el evento. Este sera proporcionado por una variable de ambiente
* Thread-Id. Nombre o identificador del hilo de la aplicación donde se produjo el evento.
* Clase. En que parte de la aplicación ocurrió el evento (o error).
* Descripción del evento:
	* Correlation Id Es un identificador utilizado para encontrar mensajes relacionados en otros microservicios.
	* Código de error (Si existe)
	* Descripción del evento
	* QueryString. La petición REST que originó el evento (Si aplica)
