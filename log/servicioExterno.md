# Notifica cuando usas un servicio externo

Para cada servicio externo registra un evento antes de su invocación y un evento inmediatamente después de haber recibido la respuesta. Esto ayudará a facilitar el diagnostico de un problema de red o de dependencia (disponibilidad) de algún servicio. Por ejemplo:

Para un servicio web:

```
LOGGER.info("Enviando {} SMSs", size);
response = wsClient.sendSMS(username, password, csvNumbers, message);
LOGGER.info("SMS enviado a {} destinatarios.", size);
```

o para una BD:

```
LOGGER.info("Registrando evento {} de la notificacion {}", eventType, correlationId);
eventRepository.save(log);
LOGGER.info("Evento con notification id {} registrado.", correlationId);
```
